-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-05-2021 a las 20:16:52
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `academia`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumnos`
--

CREATE TABLE `alumnos` (
  `alumnoDni` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `alumnoNombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `alumnoApellido` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `alumnoTelefono` char(15) COLLATE utf8_spanish2_ci NOT NULL,
  `alumnoCorreo` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `alumnoEstado` enum('Activo','Inactivo') COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `alumnos`
--

INSERT INTO `alumnos` (`alumnoDni`, `alumnoNombre`, `alumnoApellido`, `alumnoTelefono`, `alumnoCorreo`, `alumnoEstado`) VALUES
('', '', '', '', '', 'Activo'),
('67987654R', 'Adrian', 'Martinez Lopez', '623451910', 'adrianml@gmail.com', 'Inactivo'),
('70010545K', 'Miriam', 'Perez Reverte', '685541017', 'miriampr@gmail.com', 'Activo'),
('70984312F', 'Elisa', 'Suarez Velasco', '600096519', 'elisasv@gmail.com', 'Activo'),
('70991123E', 'Carmen', 'Fernandez Suarez', '666785541', 'carmenfs@gmail.com', 'Inactivo'),
('71451919E', 'Alvaro', 'Revuelta Lopez', '611267675', 'alvarorl@gmail.com', 'Inactivo'),
('71765479Z', 'Josue', 'Rodriguez Perez', '678543289', 'josuerp@gmail.com', 'Activo'),
('71890764J', 'Ana', 'Inclan Suarez', '687437889', 'anais@gmail.com', 'Activo'),
('71900021H', 'Diego', 'Paniagua Vermejo', '685542780', 'diegopv@gmail.com', 'Activo'),
('71906512W', 'Ruben', 'Rodriguez Blanco', '672306026', 'rubenrb@gmail.com', 'Activo'),
('71906512x', 'fufe', 'fafe', '6789766431', 'fafe@gmail.com', 'Activo'),
('71906515W', 'Juan', 'Rodríguez Blanco', '6789766432', 'juanrb@gmail.com', 'Activo'),
('71906789Q', 'Diego', 'Martinez Blanco', '611209854', 'diegomb@gmail.com', 'Activo'),
('72079657P', 'Silvia', 'Lechado Rodriguez', '685778897', 'silvialr@gmail.com', 'Activo'),
('72345786H', 'Susana', 'Rubio Valle', '608631132', 'susanarv@gmail.com', 'Inactivo'),
('72639954Z', 'Borja', 'Martinez Casado', '624429854', 'borjamc@gmail.com', 'Activo'),
('73729066A', 'Andres', 'Caravaca Gijon', '65049203', 'andrescg@gmail.com', 'Activo'),
('74196257E', 'Sergio', 'Cano Martinez', '613407017', 'sergiocm@gmail.com', 'Activo'),
('74337822M', 'Federico', 'Perez Villanueva', '611209666', 'federicopv@gmail.com', 'Activo'),
('75345743W', 'Jorge', 'Mendiburu Martin', '611009223', 'jorgemm@gmail.com', 'Activo'),
('75866062S', 'Maria', 'Barroso Inclan', '609099854', 'mariabi@gmail.com', 'Inactivo'),
('76346866Y', 'Maria', 'Rodriguez Fernandez', '600926818', 'mariarf@gmail.com', 'Activo'),
('77511080A', 'Carla', 'Fernandez Fajardo', '615650854', 'carlaff@gmail.com', 'Activo'),
('78741620Q', 'Ruben', 'Villanueva Lofer', '685507017', 'rubenvf@gmail.com', 'Activo'),
('78850818M', 'Fernando', 'Martinez Roldan', '622229854', 'fernandomr@gmail.com', 'Activo'),
('79443727B', 'Moises', 'Ovies Fajardo', '615555854', 'moisesof@gmail.com', 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materias`
--

CREATE TABLE `materias` (
  `materiasID` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `materiaNombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `materiasCurso` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `materiaDniProfesor` varchar(20) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `materias`
--

INSERT INTO `materias` (`materiasID`, `materiaNombre`, `materiasCurso`, `materiaDniProfesor`) VALUES
('ECO1', 'Economía y Empresa', 'FP', '11396092H'),
('ECO2', 'Economía y Empresa', 'Bachillerato', '11396092H'),
('ECO3', 'Economía y Empresa', 'UNIVERSIDAD', '11396092H'),
('INF1', 'Informatica', 'Ofimatica', '11432178J'),
('INF2', 'Informatica', 'Diseño Web', '11432178J'),
('INF6', 'Informatica', 'Sistemas operativos', '11432178J'),
('ING1', 'Ingles', 'Primaria', '11345764Y'),
('ING2', 'Ingles', 'Secundaria', '11345764Y'),
('ING3', 'Ingles', 'Bachillerato', '11345764Y'),
('LENG1', 'Lengua', 'Primaria', '11346754S'),
('LENG2', 'Lengua', 'Secundaria', '11346754S'),
('LENG3', 'Lengua', 'Bachillerato', '11346754S'),
('MAT1', 'Matematicas', 'Primaria', '11324709X'),
('MAT2', 'Matematicas', 'Secundaria', '11324709X'),
('MAT3', 'Matematicas', 'Bachillerato', '11324709X');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `matriculados`
--

CREATE TABLE `matriculados` (
  `matriculadoDni` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `matriculadoMateriaID` varchar(20) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `matriculados`
--

INSERT INTO `matriculados` (`matriculadoDni`, `matriculadoMateriaID`) VALUES
('70984312F', 'ING1'),
('70984312F', 'LENG1'),
('70984312F', 'MAT1'),
('70991123E', 'ING2'),
('71890764J', 'ING1'),
('71890764J', 'LENG1'),
('71890764J', 'MAT1'),
('71900021H', 'INF1'),
('71906789Q', 'INF1'),
('71906789Q', 'INF2'),
('74337822M', 'INF2'),
('75345743W', 'INF2'),
('78850818M', 'INF1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensajes`
--

CREATE TABLE `mensajes` (
  `mensajeID` int(255) NOT NULL,
  `mensajeMailReceptor` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `mensajeMailEmisor` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `mensajeMensaje` char(255) COLLATE utf8_spanish2_ci NOT NULL,
  `mensajesAsunto` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `mensajeAnonimo` enum('Si','No') COLLATE utf8_spanish2_ci NOT NULL,
  `mensajesFecha` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `mensajes`
--

INSERT INTO `mensajes` (`mensajeID`, `mensajeMailReceptor`, `mensajeMailEmisor`, `mensajeMensaje`, `mensajesAsunto`, `mensajeAnonimo`, `mensajesFecha`) VALUES
(2, 'secretaria@academia.com', 'web@academia.com', 'Prueba mensaje\r\n', 'Prueba asunto', 'Si', '2020-07-11 19:06:01'),
(3, 'elisasv@gmail.com', 'carlosfg@gmail.com', 'mensaje de prueba 1', 'Prueba asunto 1', 'No', '2020-07-18 18:05:56'),
(9, 'secretaria@academia.com', 'web@academia.com', 'Mensaje de prueba', 'Prueba asunto', 'Si', '2020-07-11 19:06:44'),
(10, 'secretaria@academia.com', 'web@academia.com', 'Mensaje de prueba', 'Prueba asunto', 'Si', '2020-07-11 19:06:55'),
(12, 'marcoscl@gmail.com', 'alvarorl@gmail.com', 'mensaje de prueba', 'Prueba asunto', 'No', '2020-07-11 19:06:51'),
(13, 'marcoscl@gmail.com', 'carlaff@gmail.com', 'mensaje de prueba', 'Prueba asunto', 'No', '2020-07-11 19:07:03'),
(14, 'marcoscl@gmail.com', 'rubenrb@gmail.com', 'mensaje de prueba', 'Prueba asunto', 'No', '2020-07-11 19:06:33'),
(16, 'rubenrb@gmail.com', 'marcoscl@gmail.com', 'mensaje de prueba', 'Prueba asunto', 'No', '2020-07-11 19:07:14'),
(17, 'rubenrb@gmail.com', 'marcoscl@gmail.com', 'mensaje de prueba', 'Prueba asunto', 'No', '2020-07-11 19:07:17'),
(29, 'asuncionvr@gmail.com', 'elisasv@gmail.com', 'hhh', 'prueba web', 'No', '2020-07-25 05:17:38'),
(46, 'secretaria@academia.com', 'carlosfg@gmail.com', 'ddsd', 'no funciona correctamente la w', 'Si', '2020-08-08 01:48:05'),
(49, 'secretaria@academia.com', 'maripanties@gmail.com', 'necesito ayuda', 'no funciona correctamente la w', 'Si', '2020-08-08 02:55:36'),
(50, 'secretaria@academia.com', 'maripanties@gmail.com', 'necesito ayuda', 'no funciona correctamente la w', 'Si', '2020-08-08 02:56:30'),
(51, 'secretaria@academia.com', 'maripanties@gmail.com', 'necesito ayuda', 'no funciona correctamente la w', 'Si', '2020-08-08 03:00:20'),
(52, 'secretaria@academia.com', 'maripanties@gmail.com', 'necesito ayuda', 'no funciona correctamente la w', 'Si', '2020-08-08 03:02:05'),
(53, 'secretaria@academia.com', 'maripanties@gmail.com', 'necesito ayuda', 'no funciona correctamente la w', 'Si', '2020-08-08 03:02:35'),
(54, 'secretaria@academia.com', 'maripanties@gmail.com', 'necesito ayuda', 'no funciona correctamente la w', 'Si', '2020-08-08 03:02:54'),
(55, 'secretaria@academia.com', 'foroprueba93@gmail.com', 'ggfff', 'prueba web', 'Si', '2020-08-08 03:05:44'),
(57, 'asuncionvr@gmail.com', 'carlosfg@gmail.com', 'ggbn', 'no funciona correctamente la w', 'No', '2020-08-08 04:17:26'),
(58, 'asuncionvr@gmail.com', 'antonio@gmail.com', 'kj', 'prueba web', 'No', '2020-08-22 03:52:55'),
(59, 'asuncionvr@gmail.com', 'secretaria@academia.com', 'secretario', 'secretario', 'No', '2020-08-22 03:54:15'),
(60, 'alvarorl@gmail.com', 'carlosfg@gmail.com', 'tt', 'prueba mail', 'No', '2020-12-05 07:07:55'),
(61, 'alvarorl@gmail.com', 'carlosfg@gmail.com', 'fg', 'eee', 'No', '2020-12-06 08:23:22'),
(67, 'secretaria@academia.com', 'www.rubenrb@gmail.com', 'prueba de video 1', 'prueba video', 'Si', '2020-12-09 00:17:23'),
(69, 'secretaria@academia.com', 'www.rubenrb@gmail.com', 'hjhgj', 'prueba mail', 'Si', '2020-12-10 01:15:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notas`
--

CREATE TABLE `notas` (
  `notasID` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `notasDniAlumno` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `notasIDMateria` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `nota` decimal(5,2) DEFAULT NULL,
  `Fechaultimamodificacion` timestamp NOT NULL DEFAULT current_timestamp(),
  `comentario` varchar(50) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `notas`
--

INSERT INTO `notas` (`notasID`, `notasDniAlumno`, `notasIDMateria`, `nota`, `Fechaultimamodificacion`, `comentario`) VALUES
('ing1-006', '70010545K', 'ING1', '4.00', '2020-12-09 11:18:54', 'tercer examen realizado'),
('ing2-001', '70010545K', 'ING2', '4.00', '2020-12-09 11:19:01', 'tercer examen realizado'),
('ing3-001', '70010545K', 'ING3', '6.00', '2020-12-09 11:19:16', 'Trabajo navidad'),
('ing1-002', '70984312F', 'ING1', '5.00', '2020-05-06 11:52:00', 'primer examen realizado'),
('ing1-010', '70984312F', 'ING2', '8.00', '2020-12-09 00:23:52', 'muy buen examen 2'),
('leng1-001', '70984312F', 'LENG1', '6.00', '2020-05-06 11:52:00', 'primer examen realizado'),
('mat1-001', '70984312F', 'MAT1', '8.00', '2020-05-06 11:52:00', 'primer examen realizado'),
('eco1-001', '70991123E', 'ECO1', '8.00', '2020-05-06 11:52:00', ''),
('ing2-002', '70991123E', 'ING2', '9.00', '2020-05-06 11:52:00', 'tercer examen realizado'),
('ing1-003', '71890764J', 'ING1', '5.00', '2020-05-06 11:52:00', 'segudo examen realizado'),
('leng1-002', '71890764J', 'LENG1', '5.00', '2020-05-06 11:52:00', ''),
('mat1-002', '71890764J', 'MAT1', '4.00', '2020-05-06 11:52:00', ''),
('inf1-001', '71900021H', 'INF1', '7.00', '2020-05-06 11:52:00', ''),
('inf2-001', '71906789Q', 'INF2', '8.00', '2020-05-06 11:52:00', ''),
('ing3-002', '72639954Z', 'ING3', '8.00', '2020-12-06 08:17:45', 'jklopl'),
('ing1-004', '74196257E', 'ING1', '9.00', '2020-08-15 01:21:46', 'hola'),
('ing1-005', '79443727B', 'ING1', '2.00', '2020-12-06 08:21:48', 'examen de inicio');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesores`
--

CREATE TABLE `profesores` (
  `profesorDni` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `profesorNombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `profesorApellido` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `profesorTelefono` char(15) COLLATE utf8_spanish2_ci NOT NULL,
  `profesorCorreo` varchar(50) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `profesores`
--

INSERT INTO `profesores` (`profesorDni`, `profesorNombre`, `profesorApellido`, `profesorTelefono`, `profesorCorreo`) VALUES
('11324709X', 'Luis', 'Fernandez Alonso', '617902172', 'luisfa@gmail.com'),
('11345764Y', 'Carlos', 'Fernandez Gijon', '657865543', 'carlosfg@gmail.com'),
('11346754S', 'Marta', 'Gonzalez Alonso', '600987654', 'martaga@gmail.com'),
('11396092H', 'Concepcion', 'Valero Barroso', '608470629', 'concepcionvb@gmail.com'),
('11432178J', 'Francisco', 'Fernandez Rodriguez', '6688905431', 'franciscofr@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `usuMail` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `usuPassword` varchar(200) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `usuaRol` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`usuMail`, `usuPassword`, `usuaRol`) VALUES
('', '3806bd1bc1dc23875588800dcdf7876c0962dae722658a18ec1232aedebba442ce3a32f2ae8850f420b75d71ea7e8fec1eaf90bfe6205b6973807a078a100982', 'alumno'),
('adrianml@gmail.com', '3806bd1bc1dc23875588800dcdf7876c0962dae722658a18ec1232aedebba442ce3a32f2ae8850f420b75d71ea7e8fec1eaf90bfe6205b6973807a078a100982', 'alumno'),
('alvarorl@gmail.com', 'alvarorl', 'alumno'),
('anais@gmail.com', 'anais', 'alumno'),
('andrescg@gmail.com', 'andrescg', 'alumno'),
('antonio@gmail.com', '3806bd1bc1dc23875588800dcdf7876c0962dae722658a18ec1232aedebba442ce3a32f2ae8850f420b75d71ea7e8fec1eaf90bfe6205b6973807a078a100982', 'secretario'),
('borjamc@gmail.com', 'borjamc', 'alumno'),
('carlaff@gmail.com', '3806bd1bc1dc23875588800dcdf7876c0962dae722658a18ec1232aedebba442ce3a32f2ae8850f420b75d71ea7e8fec1eaf90bfe6205b6973807a078a100982', 'alumno'),
('carlosfg@gmail.com', '3806bd1bc1dc23875588800dcdf7876c0962dae722658a18ec1232aedebba442ce3a32f2ae8850f420b75d71ea7e8fec1eaf90bfe6205b6973807a078a100982', 'profesor'),
('carmenfs@gmail.com', 'carmenfs', 'alumno'),
('cocepcionvb@gmail.com', 'cocepcionvb', 'profesor'),
('diegomb@gmail.com', 'diegomb', 'alumno'),
('diegopv@gmail.com', 'diegopv', 'alumno'),
('elisasv@gmail.com', '3806bd1bc1dc23875588800dcdf7876c0962dae722658a18ec1232aedebba442ce3a32f2ae8850f420b75d71ea7e8fec1eaf90bfe6205b6973807a078a100982', 'alumno'),
('fafe@gmail.com', '3806bd1bc1dc23875588800dcdf7876c0962dae722658a18ec1232aedebba442ce3a32f2ae8850f420b75d71ea7e8fec1eaf90bfe6205b6973807a078a100982', 'alumno'),
('federicopv@gmail.com', 'federicopv', 'alumno'),
('fernandomr@gmail.com', 'fernandomr', 'alumno'),
('jorgemm@gmail.com', 'jorjito', 'alumno'),
('josuerp@gmail.com', 'josuerp', 'alumno'),
('juanrb@gmail.com', '3806bd1bc1dc23875588800dcdf7876c0962dae722658a18ec1232aedebba442ce3a32f2ae8850f420b75d71ea7e8fec1eaf90bfe6205b6973807a078a100982', 'alumno'),
('luisfagh@gmail.com', 'luisfa', 'profesor'),
('mariabi@gmail.com', 'mariabi', 'alumno'),
('mariarf@gmail.com', 'mariarf', 'alumno'),
('martagaba@gmail.com', 'martaga', 'profesor'),
('miriampr@gmail.com', 'miriampr', 'alumno'),
('moisesof@gmail.com', 'moisesof', 'alumno'),
('rubenrb@gmail.com', 'rubenrb', 'alumno'),
('rubenvf@gmail.com', 'rubenvf', 'alumno'),
('sara@gmail.com', 'sarasecretario', 'secretario'),
('sergiocm@gmail.com', 'sergiocm', 'alumno'),
('silvialr@gmail.com', 'silvialr', 'alumno'),
('susanarv@gmail.com', 'susanarv', 'alumno'),
('vb@gmail.com', '3806bd1bc1dc23875588800dcdf7876c0962dae722658a18ec1232aedebba442ce3a32f2ae8850f420b75d71ea7e8fec1eaf90bfe6205b6973807a078a100982', 'profesor'),
('web@academia.com', 'web', 'web');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alumnos`
--
ALTER TABLE `alumnos`
  ADD PRIMARY KEY (`alumnoDni`);

--
-- Indices de la tabla `materias`
--
ALTER TABLE `materias`
  ADD PRIMARY KEY (`materiasID`),
  ADD KEY `materiaIDProfesor` (`materiaDniProfesor`);

--
-- Indices de la tabla `matriculados`
--
ALTER TABLE `matriculados`
  ADD PRIMARY KEY (`matriculadoDni`,`matriculadoMateriaID`),
  ADD KEY `matriculadoMateriaID` (`matriculadoMateriaID`);

--
-- Indices de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  ADD PRIMARY KEY (`mensajeID`),
  ADD KEY `mensajeMailReceptor` (`mensajeMailReceptor`);

--
-- Indices de la tabla `notas`
--
ALTER TABLE `notas`
  ADD PRIMARY KEY (`notasDniAlumno`,`notasIDMateria`),
  ADD KEY `notasIDMateria` (`notasIDMateria`);

--
-- Indices de la tabla `profesores`
--
ALTER TABLE `profesores`
  ADD PRIMARY KEY (`profesorDni`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`usuMail`) USING BTREE;

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  MODIFY `mensajeID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `materias`
--
ALTER TABLE `materias`
  ADD CONSTRAINT `materias_ibfk_1` FOREIGN KEY (`materiaDniProfesor`) REFERENCES `profesores` (`profesorDni`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `matriculados`
--
ALTER TABLE `matriculados`
  ADD CONSTRAINT `matriculados_ibfk_1` FOREIGN KEY (`matriculadoMateriaID`) REFERENCES `materias` (`materiasID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `matriculados_ibfk_2` FOREIGN KEY (`matriculadoDni`) REFERENCES `alumnos` (`alumnoDni`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `notas`
--
ALTER TABLE `notas`
  ADD CONSTRAINT `notas_ibfk_1` FOREIGN KEY (`notasDniAlumno`) REFERENCES `alumnos` (`alumnoDni`),
  ADD CONSTRAINT `notas_ibfk_2` FOREIGN KEY (`notasIDMateria`) REFERENCES `materias` (`materiasID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `notas_ibfk_3` FOREIGN KEY (`notasDniAlumno`) REFERENCES `alumnos` (`alumnoDni`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
