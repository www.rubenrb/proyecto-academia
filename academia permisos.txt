GRANT USAGE ON *.* TO `profesor`@`localhost` IDENTIFIED BY PASSWORD '*EF1572647A6788BD04DB43FE0F0158F7CDBBDCB3';

GRANT SELECT ON `academia`.* TO `profesor`@`localhost`;

GRANT SELECT, INSERT ON `academia`.`mensajes` TO `profesor`@`localhost`;

GRANT SELECT, INSERT, UPDATE, REFERENCES ON `academia`.`notas` TO `profesor`@`localhost`;

GRANT UPDATE (profesorTelefono) ON `academia`.`profesores` TO `profesor`@`localhost`;

GRANT SELECT, UPDATE (usuPassword) ON `academia`.`usuarios` TO `profesor`@`localhost`;


GRANT USAGE ON *.* TO `secretario`@`localhost` IDENTIFIED BY PASSWORD '*CBB2B8F7BE458F452986BB670CA98A487B1DABE6';

GRANT SELECT, INSERT, UPDATE, DELETE ON `academia`.* TO `secretario`@`localhost`;

GRANT SELECT, INSERT, UPDATE ON `academia`.`profesores` TO `secretario`@`localhost`;

GRANT SELECT, INSERT, UPDATE ON `academia`.`usuarios` TO `secretario`@`localhost`;

GRANT SELECT, INSERT, UPDATE ON `academia`.`alumnos` TO `secretario`@`localhost`;



GRANT USAGE ON *.* TO `alumno`@`localhost` IDENTIFIED BY PASSWORD '*AF6E20BA003B66DBE781BAB4977F297E3BC0CE24';

GRANT SELECT ON `cifp`.* TO `alumno`@`localhost`;

GRANT SELECT ON `academia`.* TO `alumno`@`localhost`;

GRANT INSERT ON `academia`.`mensajes` TO `alumno`@`localhost`;

GRANT UPDATE (alumnoTelefono) ON `academia`.`alumnos` TO `alumno`@`localhost`;

GRANT SELECT ON `cifp`.`usuarios` TO `alumno`@`localhost`;

GRANT SELECT ON `cifp`.`notas` TO `alumno`@`localhost`;

GRANT UPDATE (usuPassword) ON `academia`.`usuarios` TO `alumno`@`localhost`;