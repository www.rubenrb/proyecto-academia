<!DOCTYPE html>
<html>

<head>
    <title>ejercicio</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>

</head>

<style>
.jumbotron-fm{
    background-color: #3F9FC8;
    color:white;

}
body{
  background-color: #EFEEED;
}
h4,b,tr,h5{
    color:#3F9FC8;
}
#negro{
  color:black;
}

#blanco{
  color:white;
}
#principal{
    background-color:#3F9FC8; 
    color:white;
    
}


.navbar{
    padding-top: 20px;
    padding-bottom: 20px;
}

footer{
padding-left: 50px;
background-color: #E7EBED;
}

.col-centered{
  display: block;
  margin-left: auto;
  margin-right: auto;
  text-align: center;
}

#texto1{
  font-family: Century Gothic, sans-serif;
}

.carousel-inner img {
      width: 100%; 
      margin: auto;
      min-height:200px;
  }

    .top {
    background-color: #f2f2f2;
    padding: 15px;
     margin-left:30px; 
     margin-right:30px;
      margin-top:10px;
    }

   

    .bot {
      
     margin-left:20px; 
     margin-right:20px;
      
    }

#navbarSupportedContent{
  font-size: 25px;
  text-align:center;
}

#columna{
 background-color:#343a40; 

}
#negrita{
  color:black;
}
#centrado{
  text-align:center;
}
h2{
  color:white;
}
tr{
  text-align:center;
}
#horario{
  color:yellow;
}
#tarjeta{
  
  background-color:#efeeed;
}

.grid-block{
  background:no-repeat;
  background-size: cover;
        -moz-background-size: cover;
        -webkit-background-size: cover;
        -o-background-size: cover;
  opacity: 0.5;
}
#titulo{
color:#0000FF;
  font-family: "Lucida Console", Courier, monospace;
  text-align:center;
  font-size: 50px;

}
.tabla{
  padding-top: 30px;
  margin-left: auto;
margin-right: auto;

}
  #centro {
 text-align: center;
}
hr{
  border: none;
  height: 3px;
  color: #333;
  background-color: #333;
}
h1{
  text-align:center;
  color:#DF0101;
}
img{
display:block;
margin:auto;
}


</style>

<body>
<?php
  session_start();
if(isset($_SESSION["conectado"]) && isset($_SESSION["rol"])) {

  $conexion=$_SESSION["conectado"];

  $servername = "localhost";
  $username = "secretario";
  $password = "secretario";
  $dbname = "academia";

  $conn = mysqli_connect($servername, $username, $password,$dbname);

  if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
  }

  if (isset($_POST['atras'])){


      header("location:panel1.php");
  }

   if (isset($_POST['atras1'])){


      header("location:calificaciones3.php");
  }

   if (isset($_POST['cambiar'])){


      header("location:calificaciones3.php");
  }


  ?>
   <div class="container-fluid mt-3">
        <div class="row">
            <div class="col">

                <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                     <a class="navbar-brand" href="../inicio.php"><img src="../images/logo.png" alt="logo"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>


                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="../inicio.php">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Inicio <span class="sr-only">(current)</span></a>
                            </li>
                            
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    &nbsp&nbsp Nuestros servicios
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                                    <p class="dropdown-item" style="background-color:powderblue;">Cursos</p>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="economia.php">Economía y empresa</a>
                                  <a class="dropdown-item" href="lengua.php">Lengua</a>
                                    
                                    
                                    <a class="dropdown-item" href="mate.php">Matemáticas</a>
                                    
                                    
                                    <a class="dropdown-item" href="ingles.php">Inglés</a>
                                    <a class="dropdown-item" href="informatica.php">Informática</a>
                                    
                                    
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="condiciones.php">&nbsp&nbsp Condiciones</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="trabajo.php">&nbsp&nbsp Espacio de trabajo</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="contacto.php">&nbsp&nbsp Contacto</a>
                            </li>
                        </ul>
                        <?php
  
                           if(!isset($_SESSION["conectado"]) && !isset($_SESSION["rol"])) {

                          ?>
                        <form class="form-inline my-2 my-lg-0">

                            <button type="button" id="boton1" class="btn btn-primary m-1" data-toggle="modal" data-target="#modalAcceso" onclick="window.location.href='form.php'">
                                Iniciar sesión

                               
                            </button>
                  
                        </form>
                        <?php } ?>


                    </div>
                </nav>

                

            </div>
        </div>

            
      </div>  
 <?php
  if (!isset($_POST['modif'])) {

    $sql1 = "SELECT profesorDni FROM profesores WHERE profesorCorreo = '$conexion'";

    $result = mysqli_query ($conn, $sql1);

    $valor1="";
    if ($result == FALSE) {
        echo "Error en la ejecución de la consulta.<br />";
    }else{
      while ($registro = mysqli_fetch_row($result)) {
        $valor1=$registro[0];
      }
    }


    $sql5 ="SELECT materiasID FROM materias WHERE materiaDniProfesor ='$valor1'";
    $result5 = mysqli_query($conn, $sql5);

    $i = 0;
    $alts = [];
    while ($registro = mysqli_fetch_row($result5)) {
      
      $registro[0];

      $i++;
      $alts[$i] = $registro[0];

    }


    $sql2 = " SELECT notasID,notasDniAlumno,notasIDMateria, nota, Fechaultimamodificacion,comentario FROM notas WHERE notasIDMAteria='$alts[1]' OR notasIDMAteria='$alts[2]' OR notasIDMAteria='$alts[3]'";

    $result = mysqli_query($conn, $sql2);

    if ($result == TRUE) {
        ?>
            <form action="" method="post">

              <div id="centro">
                <h3>Selecciona los que quieras modificar</h3><br>
             
                <table align="center">
                    <tr>
                        <th>Modificar</th>
                        <th>ID de la nota</th>
                        <th>DNI Alumno</th>
                        <th>ID Materia</th>
                        <th>Nota</th>
                        <th>Fecha ultima modificacion</th>
                        <th>Comentario</th>
                        
                    </tr>
                    <?php
                    while ($registro = mysqli_fetch_row($result)) {
                    ?>
                    
                        <tr >
                            <td><input type="checkbox" name="checkList[]" value="<?php echo $registro[0] ?>"></td>
                            <td><?php echo $registro[0] ?></td>
                            <td><?php echo $registro[1] ?></td>
                            <td><?php echo $registro[2] ?></td>
                            <td><?php echo $registro[3] ?></td>
                            <td><?php echo $registro[4] ?></td>
                            <td><?php echo $registro[5] ?></td>
                        </tr>
                        </div>

                    <?php
    
                    }
                    ?>
                </table>
                <br><br><div id="centro">
                <button class="myButton" type="submit" name="modif"> Modificar</button>
                <input class="myButton1" type="reset" name="Submit" value="Reiniciar formulario">

                <input class="btn btn-primary" type="submit" value="Volver atras"  name="atras">

                </div>
            </form>

        <?php
    }else{
            echo "No se han encontrado Registros.";
    }
  }




?>


<?php
  if (isset($_POST['modif'])) {
          if (isset($_POST['checkList'])) {
      

              $cuenta=count($_POST['checkList']);


            if ($cuenta==1){

              foreach ($_POST['checkList'] as $selected) {
                  $sql = "SELECT notasID, nota, comentario FROM notas where notasID='$selected' ";
                  $result = mysqli_query ($conn, $sql);

                  if(mysqli_num_rows($result) > 0){
                    while ($registro = mysqli_fetch_row($result)) {

                    $_SESSION['nom']=$registro[0];

              ?>  <div id="centro">
                    <div id="texto">
                      <br><br><br><br>
                      
                        <b>Escribe los datos del nuevo registro</b></p><br>
                        <form action="" method="post">
                          
                     
                         <p><b>Nota</b></p> <input class="campo" name="nota" type="text" value="<?php echo $registro[1] ?>">
                         <br><br>
                         <p><b>Comentario</b></p> <input class="campo" name="coment" type="text" value="<?php echo $registro[2] ?>">
                        <br><br>
                         
                          

                         <input class="myButton" type="submit" value="Modificar"  name="cambiar">
                        <input class="myButton1" type="reset" name="Submit" value="Reiniciar formulario">
                        <input class="btn btn-primary" type="submit" value="Volver atras"  name="atras1">

                        </form>
                    </div>
                  </div>

              <?php           
                  
                    }
                  } 
              }
            }else{
                echo "<h3 align='center'>Escoge solo un alumno a modificar</h3>";
                ?> 
                <br><br>
                <form>
                  <div id="centro">
              <input class="btn btn-primary" type="submit" value="Volver atras"  name="atras1">
              </div>
              </form>
            <?php
            }
          }else{
            echo "<h3 align='center'>Debes escoger almenos un alumno para modificar</h3>";
            ?> 
            <br><br>
            <form>
              <div id="centro">
            <input class="btn btn-primary" type="submit" value="Volver atras"  name="atras1">
            </div>
            </form>
            <?php
          }
  }    

  if (isset($_POST['cambiar'])) {

        $n=$_SESSION['nom'];
        $f= date("Y").'-'.date("m").'-'.date("d").' '.date("h").':'.date("i").':'.date("s");

        
        $nota1=$_POST["nota"];
        $coment1=$_POST["coment"];
                    
              

        $modif = " UPDATE notas SET nota='$nota1', Fechaultimamodificacion='$f', comentario='$coment1' WHERE notasID='$n' ";
        $result1 = mysqli_query ($conn, $modif);

        if ($result1 == FALSE) {
           echo "Error en la ejecución de la consulta.<br />";

        }
                              
              
                
                          
  } 



?>


<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<footer>

    <div class="container-fluid mt-3">
        <div class="row jumbotron pt-4 pb-2">
            <div class="col-md-6">
                <h5 style="color:dimgray;" class="d-flex justify-content-center justify-content-md-start "><b>Dirección: Calle Luis Bayón Nº11 1ºDerecha</b></h5>
                <h5 style="color:dimgray;" class="d-flex justify-content-center justify-content-md-start "><b>Teléfono: 672 30 60 27</b></h5>

            </div>
            <div class="col-md-6">
                <ul class="list-inline d-flex justify-content-center justify-content-md-end">
                  <li class="list-inline-item"><img src="../img/faq.png"lass="img-fluid" width="40px" height="40px"><a href="faq.php">FAQ</a></li>
                    <li class="list-inline-item"><img src="../img/icon3.png"lass="img-fluid" width="40px" height="40px"><a href="https://www.facebook.com/">Facebook</a></li>
                    <li class="list-inline-item"><img src="../img/icon1.png"lass="img-fluid" width="40px" height="40px"><a href="https://www.instagram.com/">Instagram</a></li>
                    <li class="list-inline-item"><img src="../img/icon2.png"lass="img-fluid" width="40px" height="40px"><a href="https://twitter.com/">Twitter</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<?php
}
?>

</body>

</html>
