<!DOCTYPE html>
<html>

<head>
    <title>ejercicio</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>

</head>

<style>
.jumbotron-fm{
    background-color: #3F9FC8;
    color:white;

}
body{
  background-color: #EFEEED;
}
h4,b,tr,h5{
    color:#3F9FC8;
}
#negro{
  color:black;
}

#blanco{
  color:white;
}
#principal{
    background-color:#3F9FC8; 
    color:white;
    
}


.navbar{
    padding-top: 20px;
    padding-bottom: 20px;
}

footer{
padding-left: 50px;
background-color: #E7EBED;
}

.col-centered{
  display: block;
  margin-left: auto;
  margin-right: auto;
  text-align: center;
}

#texto1{
  font-family: Century Gothic, sans-serif;
}

.carousel-inner img {
      width: 100%; 
      margin: auto;
      min-height:200px;
  }

    .top {
    background-color: #f2f2f2;
    padding: 15px;
     margin-left:30px; 
     margin-right:30px;
      margin-top:10px;
    }

   

    .bot {
      
     margin-left:20px; 
     margin-right:20px;
      
    }

#navbarSupportedContent{
  font-size: 25px;
  text-align:center;
}

#columna{
 background-color:#343a40; 

}
#negrita{
  color:black;
}
#centrado{
  text-align:center;
}
h2{
  color:white;
}
tr{
  text-align:center;
}
#horario{
  color:yellow;
}
#tarjeta{
  
  background-color:#efeeed;
}

.grid-block{
  background:no-repeat;
  background-size: cover;
        -moz-background-size: cover;
        -webkit-background-size: cover;
        -o-background-size: cover;
  opacity: 0.5;
}
#titulo{
color:#0000FF;
  font-family: "Lucida Console", Courier, monospace;
  text-align:center;
  font-size: 50px;

}
.tabla{
  padding-top: 30px;
  margin-left: auto;
margin-right: auto;

}
hr{
  border: none;
  height: 3px;
  color: #333;
  background-color: #333;
}
h1{
  text-align:center;
  color:#DF0101;
}
img{
display:block;
margin:auto;
}


</style>

<body>
<?php
  session_start();
if(isset($_SESSION["conectado"]) && isset($_SESSION["rol"])) {

  $conexion=$_SESSION["conectado"];

  $servername = "localhost";
  $username = "secretario";
  $password = "secretario";
  $dbname = "academia";

   $conn = mysqli_connect($servername, $username, $password,$dbname);

  if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
  }

 
  if (isset($_POST['back'])){

      header("location:mensajes3.php");
  }
  if (isset($_POST['borra'])){

      header("location:mensajes3.php");
  }


  ?>
   <div class="container-fluid mt-3">
        <div class="row">
            <div class="col">

                <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                     <a class="navbar-brand" href="../inicio.php"><img src="../images/logo.png" alt="logo"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>


                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="../inicio.php">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Inicio <span class="sr-only">(current)</span></a>
                            </li>
                            
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    &nbsp&nbsp Nuestros servicios
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                                    <p class="dropdown-item" style="background-color:powderblue;">Cursos</p>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="economia.php">Economía y empresa</a>
                                  <a class="dropdown-item" href="lengua.php">Lengua</a>
                                    
                                    
                                    <a class="dropdown-item" href="mate.php">Matemáticas</a>
                                    
                                    
                                    <a class="dropdown-item" href="ingles.php">Inglés</a>
                                    <a class="dropdown-item" href="informatica.php">Informática</a>
                                    
                                    
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="condiciones.php">&nbsp&nbsp Condiciones</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="trabajo.php">&nbsp&nbsp Espacio de trabajo</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="contacto.php">&nbsp&nbsp Contacto</a>
                            </li>
                        </ul>
                        <?php
  
                           if(!isset($_SESSION["conectado"]) && !isset($_SESSION["rol"])) {

                          ?>
                        <form class="form-inline my-2 my-lg-0">

                            <button type="button" id="boton1" class="btn btn-primary m-1" data-toggle="modal" data-target="#modalAcceso" onclick="window.location.href='form.php'">
                                Iniciar sesión

                               
                            </button>
                  
                        </form>
                        <?php } ?>


                    </div>
                </nav>

                

            </div>
        </div>

            
      </div>  
 <?php
$valor1="";
  if (!isset($_POST['vermsg'])){

    $sql1 = "SELECT mensajeID, mensajeMailEmisor, mensajesAsunto, mensajesFecha FROM mensajes WHERE mensajeMailReceptor = '$conexion'";

    $result = mysqli_query ($conn, $sql1);

    
    if ($result == FALSE) {
        echo "Error en la ejecución de la consulta.<br />";
    }else {
      ?>
      <form  action="" method="post">
      <?php
        echo "<div class='tabla'>";
        echo "<table style='width:100%'>";
        echo "<tr>";
        echo "<th>"."Seleccion el mensaje que quieras leer"." <br>";
        echo "<th>" ."Asunto "."</th>";
        echo "<th>" ."Mensaje de "."</th>";
        echo "<th>" ." Fecha  "."</th>";
       
        echo "</tr>";


      while ($registro = mysqli_fetch_row($result)) {

            $_SESSION["valor1"]=$registro[0];

            echo "<tr>";
            ?>
            <td><input type="checkbox" name="checkList[]" value="<?php echo $registro[0] ?>"></td>
            <?php
            echo "<td>" .$registro[2]."</td>";
            echo "<td>" .$registro[1]."</td>";
            echo "<td>" .$registro[3]."</td>";
            ?>
            
            
           
            <?php
            
            echo "</tr>";


      }
        echo "</table>"."<br>";
        echo "</div>";
        ?>
        <br><br><div align="center">

             <button type="button" id="boton1" class="btn btn-primary m-1" data-toggle="modal" data-target="#modalAcceso" onclick="window.location.href='panel1.php'">Volver atras</button>
            
                <button class="myButton" type="submit" name="vermsg"> Leer mensaje</button>
                <button class="myButton" type="submit" name="borra"> Borrar mensaje</button>
                </div>
            </form>
            <?php
            
    }
  }
$valor1=$_SESSION["valor1"];
  if (isset($_POST['vermsg'])){
    if (isset($_POST['checkList'])) {
      $cuenta=count($_POST['checkList']);

      if ($cuenta==1){

        foreach ($_POST['checkList'] as $selected) {
          $sql2 = "SELECT mensajeMensaje, mensajeMailEmisor, mensajesAsunto, mensajesFecha FROM mensajes WHERE mensajeID = '$selected'";

          $result1 = mysqli_query ($conn, $sql2);

    
          if ($result1 == FALSE) {
            echo "Error en la ejecución de la consulta.<br />";
          }else {
       


            while ($registro = mysqli_fetch_row($result1)) {
              
                  $_SESSION["respuesta"]=$registro[1];
                  echo "<div align='center'>";
                  echo "<br><br><br>";
                  echo "<b>Asunto:</b>"." ".$registro[2]."<br>";
                  echo "<b>Mensaje de: </b>"." " . $registro[1]."<br>";
                  echo "<b>Mensaje: </b> " ." ";
                  echo $registro[0]."<br>";
                  echo "<b>Fecha del mensaje:</b> "." " . $registro[3];
                  echo "<br><br><br>";
                 
                  ?>
                 <form  action="" method="post">
                  <div align="center">
                  <input type="submit" class="btn btn-primary" name="back" value="Volver atras">
                  
                </div>
                  </form>
                  <?php
                  
                  echo "</tr>";
                  echo "</div>";

            }
        
       
          }

        }
      }else{
              echo "<br><br>";
              echo "<h3 align='center'>Escoge solo un mensaje para leer</h3>";

            ?>
            <div align="center"> 
             <button type="button" id="boton1" class="btn btn-primary m-1" data-toggle="modal" data-target="#modalAcceso" onclick="window.location.href='mensajes3.php'">Volver atras</button>
             </div>
            <?php

      }
    }else{
            echo "<br><br>";
            echo "<h3 align='center'>Debes escoger almenos un mensaje para leer</h3>";
             ?>
            <div align="center"> 
             <button type="button" id="boton1" class="btn btn-primary m-1" data-toggle="modal" data-target="#modalAcceso" onclick="window.location.href='mensajes3.php'">Volver atras</button>
             </div>
            <?php
    }
  }


  if (isset($_POST['borra'])){
    if (isset($_POST['checkList'])) {
      $cuenta=count($_POST['checkList']);

        if ($cuenta==1){

          foreach ($_POST['checkList'] as $selected1) {
            $sql3 = "DELETE from mensajes where mensajeID='$selected1'";

            $result2 = mysqli_query ($conn, $sql3);

    
              if ($result2 == FALSE) {
                echo "Error en la ejecución de la consulta.<br />";
              }
          }
        }else{
              echo "<br><br>";
              echo "<h3 align='center'>Escoge solo un mensaje para borrar</h3>";

            ?>
            <div align="center"> 
           
             </div>
            <?php

       }
    }else{
            echo "<br><br>";
            echo "<h3 align='center'>Debes escoger almenos un mensaje para borrar</h3>";
             ?>
            
            <?php
    }
  }



?>


<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<footer>

    <div class="container-fluid mt-3">
        <div class="row jumbotron pt-4 pb-2">
            <div class="col-md-6">
                <h5 style="color:dimgray;" class="d-flex justify-content-center justify-content-md-start "><b>Dirección: Calle Luis Bayón Nº11 1ºDerecha</b></h5>
                <h5 style="color:dimgray;" class="d-flex justify-content-center justify-content-md-start "><b>Teléfono: 672 30 60 27</b></h5>

            </div>
            <div class="col-md-6">
                <ul class="list-inline d-flex justify-content-center justify-content-md-end">
                  <li class="list-inline-item"><img src="../img/faq.png"lass="img-fluid" width="40px" height="40px"><a href="faq.php">FAQ</a></li>
                    <li class="list-inline-item"><img src="../img/icon3.png"lass="img-fluid" width="40px" height="40px"><a href="https://www.facebook.com/">Facebook</a></li>
                    <li class="list-inline-item"><img src="../img/icon1.png"lass="img-fluid" width="40px" height="40px"><a href="https://www.instagram.com/">Instagram</a></li>
                    <li class="list-inline-item"><img src="../img/icon2.png"lass="img-fluid" width="40px" height="40px"><a href="https://twitter.com/">Twitter</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<?php
}
?>

</body>

</html>
