<!DOCTYPE html>
<html>

<head>
    <title>ejercicio</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>

</head>

<style>
.jumbotron-fm{
    background-color: #3F9FC8;
    color:white;

}
body{
  background-color: #EFEEED;
}
h4,b,tr,h5{
    color:#3F9FC8;
}
#negro{
  color:black;
}

#blanco{
  color:white;
}
#principal{
    background-color:#3F9FC8; 
    color:white;
    
}


.navbar{
    padding-top: 20px;
    padding-bottom: 20px;
}

footer{
padding-left: 50px;
background-color: #E7EBED;
}

.col-centered{
  display: block;
  margin-left: auto;
  margin-right: auto;
  text-align: center;
}

#texto1{
  font-family: Century Gothic, sans-serif;
}

.carousel-inner img {
      width: 100%; 
      margin: auto;
      min-height:200px;
  }

    .top {
    background-color: #f2f2f2;
    padding: 15px;
     margin-left:30px; 
     margin-right:30px;
      margin-top:10px;
    }

   

    .bot {
      
     margin-left:20px; 
     margin-right:20px;
      
    }

#navbarSupportedContent{
  font-size: 25px;
  text-align:center;
}

#columna{
 background-color:#343a40; 

}
#negrita{
  color:black;
}
#centrado{
  text-align:center;
}
h2{
  color:white;
}
tr{
  text-align:center;
}
#horario{
  color:yellow;
}
#tarjeta{
  
  background-color:#efeeed;
}

.grid-block{
  background:no-repeat;
  background-size: cover;
        -moz-background-size: cover;
        -webkit-background-size: cover;
        -o-background-size: cover;
  opacity: 0.5;
}
#titulo{
color:#0000FF;
  font-family: "Lucida Console", Courier, monospace;
  text-align:center;
  font-size: 50px;

}
.tabla{
  padding-top: 30px;
  margin-left: auto;
margin-right: auto;

}
  #centro {
 text-align: center;
}
hr{
  border: none;
  height: 3px;
  color: #333;
  background-color: #333;
}
h1{
  text-align:center;
  color:#DF0101;
}
img{
display:block;
margin:auto;
}


</style>

<body>
<?php
  session_start();
if(isset($_SESSION["conectado"]) && isset($_SESSION["rol"])) {

  $conexion=$_SESSION["conectado"];

  $servername = "localhost";
  $username = "secretario";
  $password = "secretario";
  $dbname = "academia";

   $conn = mysqli_connect($servername, $username, $password,$dbname);

  if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
  }

  if (isset($_POST['atras'])){

      header("location:panel2.php");
  }

   if (isset($_POST['atras1'])){

      header("location:borraalumno.php");
  }


  if (isset($_POST['delete'])){

      
   header("location:borraalumno.php");

      
  }


  ?>
   <div class="container-fluid mt-3">
        <div class="row">
            <div class="col">

                <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                     <a class="navbar-brand" href="../inicio.php"><img src="../images/logo.png" alt="logo"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>


                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="../inicio.php">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Inicio <span class="sr-only">(current)</span></a>
                            </li>
                            
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    &nbsp&nbsp Nuestros servicios
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                                    <p class="dropdown-item" style="background-color:powderblue;">Cursos</p>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="economia.php">Economía y empresa</a>
                                  <a class="dropdown-item" href="lengua.php">Lengua</a>
                                    
                                    
                                    <a class="dropdown-item" href="mate.php">Matemáticas</a>
                                    
                                    
                                    <a class="dropdown-item" href="ingles.php">Inglés</a>
                                    <a class="dropdown-item" href="informatica.php">Informática</a>
                                    
                                    
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="condiciones.php">&nbsp&nbsp Condiciones</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="trabajo.php">&nbsp&nbsp Espacio de trabajo</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="contacto.php">&nbsp&nbsp Contacto</a>
                            </li>
                        </ul>
                        <?php
  
                           if(!isset($_SESSION["conectado"]) && !isset($_SESSION["rol"])) {

                          ?>
                        <form class="form-inline my-2 my-lg-0">

                            <button type="button" id="boton1" class="btn btn-primary m-1" data-toggle="modal" data-target="#modalAcceso" onclick="window.location.href='form.php'">
                                Iniciar sesión

                               
                            </button>
                  
                        </form>
                        <?php } ?>


                    </div>
                </nav>

                

            </div>
        </div>

            
      </div>  
 <?php
  if (!isset($_POST['delete'])) {

      $sql2 = " SELECT alumnoDni,alumnoNombre,alumnoApellido, alumnoTelefono, alumnoCorreo, alumnoEstado FROM alumnos WHERE alumnoEstado='Inactivo'";

      $result = mysqli_query($conn, $sql2);

    if ($result == TRUE) {
        ?>
            <form action="" method="post">

              <div id="centro">
                <h3>Alumnos que estan inactivos y se pueden eliminar</h3><br>
             
                <table align="center">
                    <tr>
                        <th>Modificar</th>
                        <th>Dni</th>
                        <th>Nombre</th>
                        <th>Apellidos</th>
                        <th>Teléfono</th>
                        <th>Correo</th>
                        <th>Estado</th>
                       
                        
                    </tr>
                    <?php
                    while ($registro = mysqli_fetch_row($result)) {
                       $_SESSION['mail1']=$registro[4];
                    ?>
                    
                        <tr >
                            <td><input type="checkbox" name="checkList[]" value="<?php echo $registro[4] ?>"></td>
                            <td><?php echo $registro[0] ?></td>
                            <td><?php echo $registro[1] ?></td>
                            <td><?php echo $registro[2] ?></td>
                            <td><?php echo $registro[3] ?></td>
                            <td><?php echo $registro[4] ?></td>
                            <td><?php echo $registro[5] ?></td>
                            
                            
                        </tr>
                        </div>

                    <?php
    
                    }
                    ?>
                </table>
                <br><br><div id="centro">
                <button class="myButton" type="submit" name="delete"> Borrar</button>

                <input class="myButton1" type="reset" name="Submit" value="Reiniciar formulario">

                <input class="btn btn-primary" type="submit" value="Volver atras"  name="atras">

                </div>
            </form>

        <?php
    } else {
            echo "No se han encontrado Registros.";
    }
  }




?>


<?php
  if (isset($_POST['delete'])) {
          if (isset($_POST['checkList'])) {
            $cuenta=count($_POST['checkList']);

            if ($cuenta==1){

              foreach ($_POST['checkList'] as $selected1) {
                  $_SESSION['mail3']=$selected1;
                  var_dump($selected1);
                  $sql3 = "DELETE from alumnos where alumnoCorreo='$selected1'";
                  $result2 = mysqli_query ($conn, $sql3);

    
                if ($result2 == FALSE) {
                  echo "Error en la ejecución de la consulta.<br />";
                }
              }
            }else{
              echo "<br><br>";
              echo "<h3 align='center'>Escoge solo un mensaje para borrar</h3>";

              ?>
              <div align="center"> 
             
               </div>
              <?php

            }
          }else{
            echo "<br><br>";
            echo "<h3 align='center'>Debes escoger almenos un mensaje para borrar</h3>";
             ?>
            
            <?php
          }

          $mail3=$_SESSION['mail3'];

          $sql4 = "DELETE from usuarios where usuMail='$mail3'";
          $result3 = mysqli_query ($conn, $sql4);

    
          if ($result3 == FALSE) {
            echo "Error en la ejecución de la consulta.<br />";
          }

  }




?>  
              

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<footer>

    <div class="container-fluid mt-3">
        <div class="row jumbotron pt-4 pb-2">
            <div class="col-md-6">
                <h5 style="color:dimgray;" class="d-flex justify-content-center justify-content-md-start "><b>Dirección: Calle Luis Bayón Nº11 1ºDerecha</b></h5>
                <h5 style="color:dimgray;" class="d-flex justify-content-center justify-content-md-start "><b>Teléfono: 672 30 60 27</b></h5>

            </div>
            <div class="col-md-6">
                <ul class="list-inline d-flex justify-content-center justify-content-md-end">
                  <li class="list-inline-item"><img src="../img/faq.png"lass="img-fluid" width="40px" height="40px"><a href="faq.php">FAQ</a></li>
                    <li class="list-inline-item"><img src="../img/icon3.png"lass="img-fluid" width="40px" height="40px"><a href="https://www.facebook.com/">Facebook</a></li>
                    <li class="list-inline-item"><img src="../img/icon1.png"lass="img-fluid" width="40px" height="40px"><a href="https://www.instagram.com/">Instagram</a></li>
                    <li class="list-inline-item"><img src="../img/icon2.png"lass="img-fluid" width="40px" height="40px"><a href="https://twitter.com/">Twitter</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<?php
}
?>

</body>

</html>
