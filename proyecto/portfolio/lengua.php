<!DOCTYPE html>
<html>

<head>
    <title>ejercicio</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>


</head>

<style>
.jumbotron-fm{
    background-color: #3F9FC8;
    color:white;

}
body{
  background-color: #EFEEED;
}
h4,b,tr,h5{
    color:#3F9FC8;
    text-align:center;
}
#negro{
  color:black;
}

#blanco{
  color:white;
}
#principal{
    background-color:#3F9FC8; 
    color:white;
    
}

.navbar{
    padding-top: 20px;
    padding-bottom: 20px;
}

footer{
padding-left: 50px;
background-color: #E7EBED;
}

.col-centered{
  display: block;
  margin-left: auto;
  margin-right: auto;
  text-align: center;
}

#texto1{
  font-family: Century Gothic, sans-serif;
}

.carousel-inner img {
      width: 100%; 
      margin: auto;
      min-height:200px;
  }

    .top {
    background-color: #f2f2f2;
    padding: 15px;
     margin-left:30px; 
     margin-right:30px;
      margin-top:10px;
    }

   

    .bot {
      
     margin-left:20px; 
     margin-right:20px;
      
    }

#navbarSupportedContent{
  font-size: 25px;
  text-align:center;
}

#columna{
 background-color:#343a40; 

}
#negrita{
  color:black;
}
#centrado{
  text-align:center;
}
h2{
  color:white;
}
tr{
  text-align:center;
}
#horario{
  color:yellow;
}
#tarjeta{
  
  background-color:#efeeed;
}

.grid-block{
  background:no-repeat;
  background-size: cover;
        -moz-background-size: cover;
        -webkit-background-size: cover;
        -o-background-size: cover;
  opacity: 0.5;
}
#titulo{
color:#0000FF;
  font-family: "Lucida Console", Courier, monospace;
  text-align:center;
  font-size: 50px;

}
hr{
  border: none;
  height: 3px;
  color: #333;
  background-color: #333;
}
h1{
  text-align:center;
  color:#DF0101;
}
img{
display:block;
margin:auto;
}
#negro{
  color:black;
}


</style>

<body>
  <?php
  session_start();
  ?>

    <div class="container-fluid mt-3">
        <div class="row">
            <div class="col">

                <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                     <a class="navbar-brand" href="../inicio.php"><img src="../images/logo.png" alt="logo"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>


                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="../inicio.php">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Inicio <span class="sr-only">(current)</span></a>
                            </li>
                            
                            <li class="nav-item dropdown active">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    &nbsp&nbsp Nuestros servicios
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                                    <p class="dropdown-item" style="background-color:powderblue;">Cursos</p>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="economia.php">Economía y empresa</a>
                                  <a class="dropdown-item" href="lengua.php">Lengua</a>
                                    
                                    
                                    <a class="dropdown-item" href="mate.php">Matemáticas</a>
                                    
                                    
                                    <a class="dropdown-item" href="ingles.php">Inglés</a>
                                    <a class="dropdown-item" href="informatica.php">Informática</a>
                                    
                                    
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="condiciones.php">&nbsp&nbsp Condiciones</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="trabajo.php">&nbsp&nbsp Espacio de trabajo</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="contacto.php">&nbsp&nbsp Contacto</a>
                            </li>
                        </ul>
                        <?php
  
                           if(!isset($_SESSION["conectado"]) && !isset($_SESSION["rol"])) {

                          ?>
                        <form class="form-inline my-2 my-lg-0">

                            <button type="button" id="boton1" class="btn btn-primary m-1" data-toggle="modal" data-target="#modalAcceso" onclick="window.location.href='form.php'">
                                Iniciar sesión

                               
                            </button>
                  
                        </form>
                        <?php } ?>


                    </div>
                </nav>

                

            </div>
        </div>

            
      </div>  
    
<br><br><div>

  <h4>ACADEMIA ESPECIALIZADA EN LENGUA: TODOS LOS NIVELES</h4>
<h4>PRIMARIA, ESO Y BACHILLERATO</h4>


<br><img src="../img/21.jpg" alt="clase" class="img-fluid" width="800px" height="600px">



</div><br><br>

<div class="container">

  <ul>
  <li><b>SINTAXIS:</b> Te enseñaremos a analizar sintácticamente, desde el nivel más básico a la sintaxis más avanzada y complicada. También comentario sintáctico; todo ello siempre adaptándonos al sistema de explicación de tus profesores, bién sea el método tradicional (CD, CI, CCircunstanciales, etc) o bién por el método funcional (I, C, Aditamento...). Para los exámenes de acceso  a la Universidad nos guiaremos por los criterios exigidos por la EBAU, y se practicará exahustivamente con los exámenes de años anteriores.</li>
  <li><b>MORFOLOGÍA:</b>  Todos los tipos de palabras y su función, separación lexema-morfemas y comentario morfológico.</li>
  <li><b>COMENTARIO DE TEXTOS:</b> Tanto de tipo ensayístico, o tema de actualidad como de tipo literario- lírico, también teniendo en cuenta siempre las indicaciones que hallas recibido del profesorado,  su modelo de comentario a seguir, o los criterios EBAU en su caso.</li>
  <li><b>REFORMULACIÓN LÉXICA, ANÁLISIS MÉTRICO</b>, y todos los tipos de prácticas que sean necesarias para preparar tu examen.</li>
</ul> <br>
  <h2 id="negro">NIVELES IMPARTIDOS</h2>
  
  <table class="table table-dark table-striped">
    <thead>
      <tr>
        <th>NIVELES</th>
        <th>ASIGNATURAS</th>
        
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>PRIMARIA</td>
        <td>LENGUA</td>
        
      </tr>
      <tr>
        <td>SECUNDARIA / ESO</td>
        <td>LENGUA Y LITERATURA</td>
        
      </tr>
      <tr>
        <td>BACHILLERATO</td>
        <td>LENGUA Y LITERATURA</td>
        
      </tr>
    </tbody>
  </table><br>
  <p>Muchos alumos presentan problemas de base en esta materia, asi que estamos acostumbrados a explicar desde niveles muy bajos; no sólo es fundamental aprobar los exámenes de Lengua, sino que  también es importante  subir la nota de estos exámenes, pues al ser asignatura siempre obligatoria y troncal, una buena nota mejora mucho la media total, e incluso consigue el aprobado de muchas pruebas obligatorias para el acceso a niveles superiores de enseñanza o la a la Universidad. Confía en nuestros años de experiencia en clases de Lengua.
    Cuenta con nosotros también para preparar <u>otras asignaturas</u>, de varios <u>cursos y niveles</u>, siempre con <u>horarios</u> flexibles, y una <u>situación</u> privilegiada respecto a las comunicaciones.</p>

</div><br><br>


<br>
<footer>

    <div class="container-fluid mt-3">
        <div class="row jumbotron pt-4 pb-2">
            <div class="col-md-6">
                <h5 style="color:dimgray;" class="d-flex justify-content-center justify-content-md-start "><b>Dirección: Calle Luis Bayón Nº11 1ºDerecha</b></h5>
                <h5 style="color:dimgray;" class="d-flex justify-content-center justify-content-md-start "><b>Teléfono: 672 30 60 27</b></h5>

            </div>
            <div class="col-md-6">
                <ul class="list-inline d-flex justify-content-center justify-content-md-end">
                  <li class="list-inline-item"><img src="../img/faq.png"lass="img-fluid" width="40px" height="40px"><a href="faq.php">FAQ</a></li>
                    <li class="list-inline-item"><img src="../img/icon3.png"lass="img-fluid" width="40px" height="40px"><a href="https://www.facebook.com/">Facebook</a></li>
                    <li class="list-inline-item"><img src="../img/icon1.png"lass="img-fluid" width="40px" height="40px"><a href="https://www.instagram.com/">Instagram</a></li>
                    <li class="list-inline-item"><img src="../img/icon2.png"lass="img-fluid" width="40px" height="40px"><a href="https://twitter.com/">Twitter</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>



</body>

</html>
