<!DOCTYPE html>
<html>

<head>
    <title>ejercicio</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>

</head>

<style>
.jumbotron-fm{
    background-color: #3F9FC8;
    color:white;

}
body{
  background-color: #EFEEED;
}
h4,b,tr,h5{
     color:#3F9FC8;
}
#negro{
  color:black;
}

#blanco{
  color:white;
}
#principal{
    background-color:#3F9FC8; 
    color:white;
    
}

.navbar{
    padding-top: 20px;
    padding-bottom: 20px;
}

footer{
padding-left: 50px;
background-color: #E7EBED;
}

.col-centered{
  display: block;
  margin-left: auto;
  margin-right: auto;
  text-align: center;
}

#texto1{
  font-family: Century Gothic, sans-serif;
}

.carousel-inner img {
      width: 100%; 
      margin: auto;
      min-height:200px;
  }

    .top {
    background-color: #f2f2f2;
    padding: 15px;
     margin-left:30px; 
     margin-right:30px;
      margin-top:10px;
    }

   

    .bot {
      
     margin-left:20px; 
     margin-right:20px;
      
    }

#navbarSupportedContent{
  font-size: 25px;
  text-align:center;
}

#columna{
 background-color:#343a40; 

}
#negrita{
  color:black;
}
#centrado{
  text-align:center;
}
h2{
  color:white;
}
tr{
  text-align:center;
}
#horario{
  color:yellow;
}
#tarjeta{
  
  background-color:#efeeed;
}

.grid-block{
  background:no-repeat;
  background-size: cover;
        -moz-background-size: cover;
        -webkit-background-size: cover;
        -o-background-size: cover;
  opacity: 0.5;
}
#titulo{
color:#0000FF;
  font-family: "Lucida Console", Courier, monospace;
  text-align:center;
  font-size: 50px;

}
img{
display:block;
margin:auto;
}
#parrafo1{
  font-size:20px;
}
#titulos{
   color:#3F9FC8;
}
#parrafo{
  margin-left:40px;
  margin-right:40px;
}


</style>

<body>
<?php
  session_start();
  ?>
    <div class="container-fluid mt-3">
        <div class="row">
            <div class="col">

                <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                     <a class="navbar-brand" href="../inicio.php"><img src="../images/logo.png" alt="logo"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>


                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="../inicio.php">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Inicio <span class="sr-only">(current)</span></a>
                            </li>
                            
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    &nbsp&nbsp Nuestros servicios
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                                    <p class="dropdown-item" style="background-color:powderblue;">Cursos</p>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="economia.php">Economía y empresa</a>
                                  <a class="dropdown-item" href="lengua.php">Lengua</a>
                                    
                                    
                                    <a class="dropdown-item" href="mate.php">Matemáticas</a>
                                    
                                    
                                    <a class="dropdown-item" href="ingles.php">Inglés</a>
                                    <a class="dropdown-item" href="informatica.php">Informática</a>
                                    
                                    
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="condiciones.php">&nbsp&nbsp Condiciones</a>
                            </li>
                            <li class="active">
                                <a class="nav-link" href="trabajo.php">&nbsp&nbsp Espacio de trabajo</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="contacto.php">&nbsp&nbsp Contacto</a>
                            </li>
                        </ul>
                        <?php
  
                           if(!isset($_SESSION["conectado"]) && !isset($_SESSION["rol"])) {

                          ?>
                        <form class="form-inline my-2 my-lg-0">

                            <button type="button" id="boton1" class="btn btn-primary m-1" data-toggle="modal" data-target="#modalAcceso" onclick="window.location.href='form.php'">
                                Iniciar sesión

                               
                            </button>
                  
                        </form>
                        <?php } ?>


                    </div>
                </nav>

                

            </div>
        </div>

            
      </div> 
 <div class="m-5">
     <br><br><div id="parrafo">  
    
    <h4 id="titulos">El estudio dirigido es un plan de trabajo o técnica para guiar al alumno en los métodos de estudio y de pensamiento reflexivo</h4><br>

    <p id="parrafo1">Nuestro método de Estudio Dirigido consiste en hacer que el alumno, trabaje diariamente una o varias asignaturas, supervisado por un profesor/tutor que planifica el estudio, corrige el trabajo realizado y resuelve las dudas que el alumno pueda plantear, con la extensión y profundidad necesarias para superar los exámenes de cada asignatura.</p>
 
     </div><br><hr> 


      <br><br><div id="parrafo">  
    
    <h4 id="titulos">Objetivos:</h4><br>

      <ul id="parrafo1">
        <li>Enseñar al alumno a aprender por sí mismo con mínima orientación del docente y practicar la autonomía.</li>
        <li>Estimular su pensamiento reflexivo, su capacidad de síntesis, comprensión, resumen, determinación de conceptos, resolución de problemas, análisis, etc.</li>
        <li>Sacar al alumno de la pasividad.</li>
        <li>Ejercitarlo en el uso de herramientas y técnicas de estudio.</li>
        <li>Favorecer el trabajo de los alumnos, dado que cada uno estudia con su propio ritmo.</li>
      </ul> 
 
     </div><br><hr> 

        <div class="row p-3">
          <div class="col-sm-4 col-md-3"><img src="../img/13.jpg" alt="clase" class="img-fluid" width="400px" height="200px"></div>
          <div class="col-sm-8 col-md-9">

            <br><h4 id="titulos">Aula de Estudio </h4><br>

            <p id="parrafo1">Un ambiente agradable, bien iluminado y estimulante mejora nuestra disposición al estudio.<br><br>
              La academia Olimpo trata de romper con la imagen de academia tradicional y crea un espacio para la inmersión en el aprendizaje.<br><br>
              Puedes consultar libros y materiales mientra disfrutas de una bebida (agua, café, infusión, té…).<br><br>
              Nuestras aulas están equipadas con pizarras, proyector, ordenador, amplias mesas, y mucho más.</p>

          </div>
        </div>
        <hr class="bg-light mt-1 mt-md-3">


         <div class="row p-3">
          <div class="col-sm-4 col-md-3"><img src="../img/14.jpg" alt="clase" class="img-fluid" width="400px" height="200px"></div>
          <div class="col-sm-8 col-md-9">

            <br><h4 id="titulos">Duración </h4><br>

            <p id="parrafo1">El estudio dirigido como método de enseñanza,tiene lugar en un aula adaptada a tal efecto y dentro del horario extra-escolar.<br><br>

              Las sesiones tienen una duración de 60 min. impartidas de lunes a viernes en jornada tanto de mañanas como de tardes.</p>

          </div>
        </div>
        <hr class="bg-light mt-1 mt-md-3">

           <div id="parrafo">  

            <br><h4 id="titulos">Metodología </h4><br>

            <p id="parrafo1">Nuestro método de estudio dirigido se desarrolla en grupos reducidos – máx. 8 alumnos bachiller, 6 alumnos E.S.O. -, tiene lugar en nuestras aulas y con presencia de un Profesor-tutor que supervisa la sesión e informa semanalmente a los padres del trabajo realizado y el trabajo que ha quedado pendiente.<br><br>

            En nuestras aulas los alumnos trabajan siguiendo los contenidos, el ritmo y los materiales establecidos por el centro en el que cursa estudios.<br><br>

            Los alumnos, cuando entran en el Aula, informan a su profesor de los contenidos vistos en clase y de la tarea pendiente de preparar. El profesor realiza una distribución realista del trabajo a realizar por cada alumno, priorizando el trabajo en aquellas materias que requieren de refuerzo personalizado.<br><br>

            Durante las sesiones de Estudio Dirigido, el alumno estudia de forma individual, preparando las tareas y trabajos que le han sido encomendados en clase por sus profesores titulares y que deben estar reflejadas en la agenda de cada alumno.<br><br>

            Una vez identificado el trabajo a realizar, cada alumno se pone a trabajar en silencio, sabiendo que, en caso de presentarse dificultades que no sean capaces de resolver por ellos mismos, podrán solicitar la ayuda del profesor que los ayudará individualmente, no resolviendo la dificultad, sino señalando caminos que lleven a superarla.<br><br>

            A medida que el alumno finaliza las tareas programadas en cada asignatura, el profesor comprueba que efectivamente se han realizado de forma correcta. Al finalizar la semana, el profesor redacta un informe en el que comunica a los padres el trabajo realizado en clase de cada asignatura y las tareas que han quedado pendientes para el fin de semana, si las hubiera.</p><br><br>

            <img src="../img/17.jpg" alt="clase" class="img-fluid" width="700px" height="500px">

          </div>
        
        <hr class="bg-light mt-1 mt-md-3">


<br>
<footer>

    <div class="container-fluid mt-3">
        <div class="row jumbotron pt-4 pb-2">
            <div class="col-md-6">
                <h5 style="color:dimgray;" class="d-flex justify-content-center justify-content-md-start "><b>Dirección: Calle Luis Bayón Nº11 1ºDerecha</b></h5>
                <h5 style="color:dimgray;" class="d-flex justify-content-center justify-content-md-start "><b>Teléfono: 672 30 60 27</b></h5>

            </div>
            <div class="col-md-6">
                <ul class="list-inline d-flex justify-content-center justify-content-md-end">
                  <li class="list-inline-item"><img src="../img/faq.png"lass="img-fluid" width="40px" height="40px"><a href="faq.php">FAQ</a></li>
                    <li class="list-inline-item"><img src="../img/icon3.png"lass="img-fluid" width="40px" height="40px"><a href="https://www.facebook.com/">Facebook</a></li>
                    <li class="list-inline-item"><img src="../img/icon1.png"lass="img-fluid" width="40px" height="40px"><a href="https://www.instagram.com/">Instagram</a></li>
                    <li class="list-inline-item"><img src="../img/icon2.png"lass="img-fluid" width="40px" height="40px"><a href="https://twitter.com/">Twitter</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>



</body>

</html>
