<!DOCTYPE html>
<html>

<head>
    <title>ejercicio</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>

  <link rel="stylesheet" type="text/css" href="../vendor/bootstrap/css/bootstrap.min.css">

  <link rel="stylesheet" type="text/css" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">

  <link rel="stylesheet" type="text/css" href="../vendor/animate/animate.css">

  <link rel="stylesheet" type="text/css" href="../vendor/css-hamburgers/hamburgers.min.css">

  <link rel="stylesheet" type="text/css" href="../vendor/select2/select2.min.css">

  <link rel="stylesheet" type="text/css" href="../css/util.css">
  <link rel="stylesheet" type="text/css" href="../css/main.css">



</head>

<style>

  #rojo{
  color:#EE280C;
}
.jumbotron-fm{
    background-color: #3F9FC8;
    color:white;

}
body{
  background-color: #EFEEED;
}
h4,b,tr,h5{
    color:#3F9FC8;
}
#negro{
  color:black;
}

#blanco{
  color:white;
}
#principal{
    background-color:#3F9FC8; 
    color:white;
    
}

.navbar{
    padding-top: 20px;
    padding-bottom: 20px;
}

footer{
padding-left: 50px;
background-color: #E7EBED;
}

.col-centered{
  display: block;
  margin-left: auto;
  margin-right: auto;
  text-align: center;
}

#texto1{
  font-family: Century Gothic, sans-serif;
}

.carousel-inner img {
      width: 100%; 
      margin: auto;
      min-height:200px;
  }

    .top {
    background-color: #f2f2f2;
    padding: 15px;
     margin-left:30px; 
     margin-right:30px;
      margin-top:10px;
    }

   

    .bot {
      
     margin-left:20px; 
     margin-right:20px;
      
    }

#navbarSupportedContent{
  font-size: 25px;
  text-align:center;
}

#columna{
 background-color:#343a40; 

}
#negrita{
  color:black;
}
#centrado{
  text-align:center;
}
h2{
  color:white;
}
tr{
  text-align:center;
}
#horario{
  color:yellow;
}
#tarjeta{
  
  background-color:#efeeed;
}
img{
display:block;
margin:auto;
}


</style>

<body>

<?php
  

  session_start();
  $servername = "localhost";
  $username = "secretario";
  $password = "secretario";
  $dbname = "academia";

   $conn = mysqli_connect($servername, $username, $password,$dbname);

  if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
  }


  $emailerr=$asunterr=$menserr = "";
  $email1=$msg1=$asunt1="";
  $emailerr=$asunterr=$menserr=false;


  if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if(empty($_POST["email"])) {
        $emailerr = "Email necesario";
    }else{
         $email1 = true;
    }

    if(empty($_POST["asunto"])) {
        $asunterr = "Asunto necesario";
    }else{
         $asunt1 = true;
    }

    if(empty($_POST["message"])) {
      $menserr = "Mensaje necesario";
    }else{
         $msg1 = true;
    }

  }
  
  ?>

    <div class="container-fluid mt-3">
        <div class="row">
            <div class="col">

                <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                     <a class="navbar-brand" href="../inicio.php"><img src="../images/logo.png" alt="logo"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>


                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="../inicio.php">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Inicio <span class="sr-only">(current)</span></a>
                            </li>
                            
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    &nbsp&nbsp Nuestros servicios
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                                    <p class="dropdown-item" style="background-color:powderblue;">Cursos</p>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="economia.php">Economía y empresa</a>
                                  <a class="dropdown-item" href="lengua.php">Lengua</a>
                                    
                                    
                                    <a class="dropdown-item" href="mate.php">Matemáticas</a>
                                    
                                    
                                    <a class="dropdown-item" href="ingles.php">Inglés</a>
                                    <a class="dropdown-item" href="informatica.php">Informática</a>
                                    
                                    
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="condiciones.php">&nbsp&nbsp Condiciones</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="trabajo.php">&nbsp&nbsp Espacio de trabajo</a>
                            </li>
                            <li class="active">
                                <a class="nav-link" href="contacto.php">&nbsp&nbsp Contacto</a>
                            </li>
                        </ul>
                        <?php
  
                           if(!isset($_SESSION["conectado"]) && !isset($_SESSION["rol"])) {

                          ?>
                        <form class="form-inline my-2 my-lg-0">

                            <button type="button" id="boton1" class="btn btn-primary m-1" data-toggle="modal" data-target="#modalAcceso" onclick="window.location.href='form.php'">
                                Iniciar sesión

                               
                            </button>
                  
                        </form>
                        <?php } ?>


                    </div>
                </nav>

                

            </div>
        </div>

     </div>
 <div class="m-5">
     <div class="bg-contact2" style="background-image: url('images/bg-01.jpg');">
    <div class="container-contact2">
      <div class="wrap-contact2">
        <form action="<?php ($_SERVER["PHP_SELF"]);?>" method="post" class="contact2-form validate-form"> 
          <span class="contact2-form-title">
            Póngase en contacto 

          </span>
          <p align="center">No dudes en contactar con nosotros para ampliar información. Estamos encantados de ayudarte.</p><br>


          <div class="wrap-input2 validate-input" data-validate="Asunto es requerido"><br>
            <input class="input2" type="text" name="asunto">
            <span class="focus-input2" data-placeholder="Asunto"></span>
            <span id="rojo">*<br> <?php echo $asunterr;?></span>
          </div>

          <div class="wrap-input2 validate-input" data-validate = "Email valido es requerido: ex@abc.xyz"><br>
            <input class="input2" type="text" name="email">
            <span class="focus-input2" data-placeholder="EMAIL"></span>
             <span id="rojo">*<br> <?php echo $emailerr;?></span>
          </div>

          <div class="wrap-input2 validate-input" data-validate = "Mensaje es requerido"><br>
            <textarea class="input2" name="message"></textarea>
            <span class="focus-input2" data-placeholder="Mensaje"></span>
            <span id="rojo">*<br> <?php echo $menserr;?></span>

          </div>

          <div class="container-contact2-form-btn">
            <div class="wrap-contact2-form-btn">
              <div class="contact2-form-bgbtn"></div>
              <input type="submit" class="btn btn-primary" name="enviar" value="Enviar mensaje">
              
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
   

<?php

if(($email1==true)&&($msg1==true)&&($asunt1==true)){
  if (isset($_POST['enviar'])){

    $f= date("Y").'-'.date("m").'-'.date("d").' '.date("h").':'.date("i").':'.date("s");

     $asunto=$_POST["asunto"];
     $email=$_POST["email"];
     $mensaje=$_POST["message"];
     $mail2="secretaria@academia.com";
    

     $sql4= "INSERT INTO mensajes (mensajeID,mensajeMailReceptor,mensajeMailEmisor,mensajeMensaje,mensajesAsunto,mensajeAnonimo,mensajesFecha) VALUES (NULL,'$mail2','$email','$mensaje','$asunto','Si','$f')";
     

     $result6 = mysqli_query ($conn, $sql4);

      
    if ($result6 == FALSE) {
       
          echo "Error en la ejecución de la consulta.<br />";
          
    }else{
          echo "<div align='center'>";
          echo "<b>Se ha enviado el mensaje</b>";
          echo "</div>";
    }

  }
}


  ?>

<br>
<footer>

<div class="container-fluid mt-3">
        <div class="row jumbotron pt-4 pb-2">
            <div class="col-md-6">
                <h5 style="color:dimgray;" class="d-flex justify-content-center justify-content-md-start "><b>Dirección: Calle Luis Bayón Nº11 1ºDerecha</b></h5>
                <h5 style="color:dimgray;" class="d-flex justify-content-center justify-content-md-start "><b>Teléfono: 672 30 60 27</b></h5>

            </div>
            <div class="col-md-6">
                <ul class="list-inline d-flex justify-content-center justify-content-md-end">
                  <li class="list-inline-item"><img src="../img/faq.png"lass="img-fluid" width="40px" height="40px"><a href="faq.php">FAQ</a></li>
                    <li class="list-inline-item"><img src="../img/icon3.png"lass="img-fluid" width="40px" height="40px"><a href="https://www.facebook.com/">Facebook</a></li>
                    <li class="list-inline-item"><img src="../img/icon1.png"lass="img-fluid" width="40px" height="40px"><a href="https://www.instagram.com/">Instagram</a></li>
                    <li class="list-inline-item"><img src="../img/icon2.png"lass="img-fluid" width="40px" height="40px"><a href="https://twitter.com/">Twitter</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>




 


</body>

</html>
