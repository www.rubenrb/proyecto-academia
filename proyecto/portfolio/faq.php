<!DOCTYPE html>
<html>

<head>
    <title>ejercicio</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>

</head>

<style>
.jumbotron-fm{
    background-color: #3F9FC8;
    color:white;

}
body{
  background-color: #EFEEED;
}
h4,b,tr,h5{
    color:#3F9FC8;
}
#negro{
  color:black;
}

#blanco{
  color:white;
}
#principal{
    background-color:#3F9FC8; 
    color:white;
    
}

.navbar{
    padding-top: 20px;
    padding-bottom: 20px;
}

footer{
padding-left: 50px;
background-color: #E7EBED;
}

.col-centered{
  display: block;
  margin-left: auto;
  margin-right: auto;
  text-align: center;
}

#texto1{
  font-family: Century Gothic, sans-serif;
}

.carousel-inner img {
      width: 100%; 
      margin: auto;
      min-height:200px;
  }

    .top {
    background-color: #f2f2f2;
    padding: 15px;
     margin-left:30px; 
     margin-right:30px;
      margin-top:10px;
    }

   

    .bot {
      
     margin-left:20px; 
     margin-right:20px;
      
    }

#navbarSupportedContent{
  font-size: 25px;
  text-align:center;
}

#columna{
 background-color:#343a40; 

}
#negrita{
  color:black;
}
#centrado{
  text-align:center;
}

tr{
  text-align:center;
}
#horario{
  color:yellow;
}
#tarjeta{
  
  background-color:#efeeed;
}

.grid-block{
  background:no-repeat;
  background-size: cover;
        -moz-background-size: cover;
        -webkit-background-size: cover;
        -o-background-size: cover;
  opacity: 0.5;
}
#titulo{
color:#0000FF;
  font-family: "Lucida Console", Courier, monospace;
  text-align:center;
  font-size: 50px;

}
hr{
  border: none;
  height: 3px;
  color: #333;
  background-color: #333;
}
h1{
  text-align:center;
  color:#DF0101;
}
img{
display:block;
margin:auto;
}

h2 {
    font-family: Arial, Verdana;
    font-weight: 800;
    font-size: 2.5rem;
    color: #091f2f;
    text-transform: uppercase;
}
.accordion-section .panel-default > .panel-heading {
    border: 0;
    background: #f4f4f4;
    padding: 0;
}
.accordion-section .panel-default .panel-title a {
    display: block;
    font-style: italic;
    font-size: 1.5rem;
}
.accordion-section .panel-default .panel-title a:after {
    font-family: 'FontAwesome';
    font-style: normal;
    font-size: 3rem;
    content: "\f106";
    color: #1f7de2;
    float: right;
    margin-top: -12px;
}
.accordion-section .panel-default .panel-title a.collapsed:after {
    content: "\f107";
}
.accordion-section .panel-default .panel-body {
    font-size: 1.2rem;
}


</style>

<body>
  <?php
    session_start();
  ?>

     <div class="container-fluid mt-3">
        <div class="row">
            <div class="col">

                <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                     <a class="navbar-brand" href="../inicio.php"><img src="../images/logo.png" alt="logo"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>


                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="../inicio.php">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Inicio <span class="sr-only">(current)</span></a>
                            </li>
                            
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    &nbsp&nbsp Nuestros servicios
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                                    <p class="dropdown-item" style="background-color:powderblue;">Cursos</p>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="economia.php">Economía y empresa</a>
                                  <a class="dropdown-item" href="lengua.php">Lengua</a>
                                    
                                    
                                    <a class="dropdown-item" href="mate.php">Matemáticas</a>
                                    
                                    
                                    <a class="dropdown-item" href="ingles.php">Inglés</a>
                                    <a class="dropdown-item" href="informatica.php">Informática</a>
                                    
                                    
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="condiciones.php">&nbsp&nbsp Condiciones</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="trabajo.php">&nbsp&nbsp Espacio de trabajo</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="contacto.php">&nbsp&nbsp Contacto</a>
                            </li>
                        </ul>
                        <?php
  
                           if(!isset($_SESSION["conectado"]) && !isset($_SESSION["rol"])) {

                          ?>
                        <form class="form-inline my-2 my-lg-0">

                            <button type="button" id="boton1" class="btn btn-primary m-1" data-toggle="modal" data-target="#modalAcceso" onclick="window.location.href='form.php'">
                                Iniciar sesión

                               
                            </button>
                  
                        </form>
                        <?php } ?>


                    </div>
                </nav>

                

            </div>
        </div>

            
      </div>  <br>


    <section class="accordion-section clearfix mt-3" aria-label="Question Accordions">
  <div class="container">
  
    <h2>Preguntas frecuentes </h2>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
      <div class="panel-heading p-3 mb-3" role="tab" id="heading0">
      <h3 class="panel-title">
        <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse0" aria-expanded="true" aria-controls="collapse0">
        ¿Son convenientes las clases particulares? 
        </a>
      </h3>
      </div>
      <div id="collapse0" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading0">
      <div class="panel-body px-3 mb-4">
        <p>Aunque la pregunta es muy ambigua, genérica y, en consecuencia, nada precisa, vamos a intentar dar una respuesta útil para cualquier alumno que se la haga o cualquier padre o madre a quien, en algún momento, se le haya pasado por la cabeza este interrogante y haya tenido la duda de si eso de las clases particulares es algo que beneficie o, por el contrario, perjudique o, en definitiva, no sirva para nada.</p>
        <p>Cuando a la academia olimpo acude una familia solicitando, por ejemplo, clases particulares de matemáticas,  de inglés,  lengua… para alguno de sus hijos, nuestro primer objetivo es averiguar si el alumno para el que se solicitan las clases, las necesita o no. Nuestra principal tarea es de asesoramiento y asesorar es lo primero que debemos hacer.</p>

        <br><h4>¿CÓMO AVERIGUAR SI UN ALUMNO NECESITA O NO CLASES PARTICULARES?</h4><br>
        <p><b>Conociendo su nivel.</b> Si un alumno carece del nivel que le corresponde, probablemente, “se pierda” ante las explicaciones en clase. Necesita recuperar el nivel perdido por lo que es posible que necesite de ayuda externa para alcanzarlo.</p>
        <p><b>Conociendo a su profesor titular.</b> Existen profesores buenos y profesores malos, muy exigentes y excesivamente blandos, que se explican como un libro abierto o que para entenderlos habría que echar mano del mejor de los intérpretes. Ante un profesor que no es capaz de transmitir con claridad los conocimientos que el alumno necesita tal vez convenga la ayuda de un profesor particular que cubra esa carencia. Ante un profesor excesivamente exigente, quizás resulte necesario un “empujón” por parte de alguien que, de manera más directa, más “particular”, pueda echarnos una mano para alcanzar el nivel exigible.</p>
        <p><b>Conociendo la actitud del alumno en clase.</b> ¿De qué sirven clases particulares si en el colegio, en el instituto el alumno está ausente, desconecta, interrumpe el ritmo de la clase, incluso, no asiste en muchas ocasiones? ¿Podemos sustituir esa actitud mediante clases particulares? La respuesta es clara: no.</p>
        <p><b>Conociendo la capacidad del propio alumno.</b> No todas las personas somos iguales y, en consecuencia, nuestra capacidad difiere de unos a otros. Hay alumnos que, aunque se esfuerzan y quieren, hay algo en su cerebro que les impide llegar. ¿Necesitan ayuda? Claro que sí. Les estamos exigiendo algo que, por sí mismos, no pueden conseguir. Unas clases particulares podrían ser, por qué no, la solución.</p>
        <p><b>Conociendo su aptitud hacia las asignaturas.</b> Un alumno puede ser bueno en la asignatura de Historia y “un desastre” en la de matemáticas; o al contrario. A veces la parte de nuestro cerebro más receptiva a las matemáticas está más o menos desarrollada que la que tiene que ver con las lenguas o la filosofía.</p>
        <p><b>Conociendo el hábito y la forma de estudio del alumno.</b> El estudio es un ejercicio mental y, como tal, requiere un entrenamiento. Pero, como todo buen entrenamiento, ha de hacerse atendiendo a una serie de estrategias, a las llamadas técnicas de estudio. No es que se nos den mal las matemáticas, el inglés, la física o la química… es, sencillamente, que no sabemos estudiar y, si no sabemos estudiar, las clases particulares de una asignatura concreta nos servirán, pero de poco.</p>
        <p>En definitiva, las clases particulares sólo son buenas si, en realidad, se necesitan. Es necesario, pues, hacer un diagnóstico claro de las necesidades del alumno. Averiguar el origen de sus carencias para poder determinar si le convienen o, por el contrario, no está en las clases particulares la solución al problema.</p>

      </div>
      </div>
    </div>
    
    <div class="panel panel-default">
      <div class="panel-heading p-3 mb-3" role="tab" id="heading1">
      <h3 class="panel-title">
        <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
        Ventajas de las clases particulares
        </a>
      </h3>
      </div>
      <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
      <div class="panel-body px-3 mb-4">
        <p><b>Resolver dudas.</b> A veces, por mucho que el alumno lo intente, por mucho que se esfuerce, ha tenido la mala suerte de tocarle ese profesor que, aún poseedor de un rico bagaje en conocimientos de su materia, no sabe transmitirlos. Saber enseñar no es fácil y eso lo sabemos muy todos aquellos que nos dedicamos a la docencia. Podemos, como decimos, estar atentos, preguntar una y otra vez, que el resultado será quedarnos como estábamos. ¿A quién acudir, entonces? A un profesor particular que sea capaz de hacernos entender lo que nos resulta imposible en el colegio o instituto.</p>
        <p><b>Comprobar nuestro esfuerzo y rendimiento.</b> En una clase de 30, 25 o incluso de 20 alumnos, el profesor no puede, aunque quiera, llevar a cabo un seguimiento directo de todos y cada uno de los alumnos. Un profesor particular sí. Muchos alumnos lo necesitan. Sentirse controlados no es malo; nos “obliga” a cumplir nuestros compromisos.</p>
        <p><b>Motivar.</b> El principal objetivo de un alumno cuando estudia matemáticas, lengua, biología, etc. es aprobar.  Ése es ya un “motivo” importante por el que esforzarnos. Ese objetivo es también compartido por el profesor particular. Ambos, profesor y alumno, tienen, pues, un objetivo común, un motivo por el que trabajar. Pero para aprobar es necesario entender las explicaciones, procesar correctamente toda la información recibida por lo que, cada vez que el alumno recibe una clase particular, tiene un objetivo inmediato, existe un motivo que justifica esa clase: entender. Cumplido el objetivo el alumno se siente motivado y éste es un aspecto crucial en todo proceso de aprendizaje.</p>
        <p><b>Ganar en seguridad y confianza.</b> La falta de confianza en nosotros mismos hace que, ante el más mínimo esfuerzo, lejos de intentar superarlo, abandonemos. Un alumno al que se le atraviesa una asignatura probablemente piense que lo tiene perdido por mucho que pretenda aprobarla. No sólo no estudia en casa, tampoco en clase del colegio adopta la actitud más favorable. Desconecta. Piensa ¿para qué esforzarme si no voy a entenderlo? El objetivo de una clase particular es, como hemos dicho, que el alumno entienda aquello que, antes de comenzar, no entendía. Al final de la clase lo ha conseguido, pero no en una ocasión, sino siempre que recibe las clases. Confía en sí mismo, cree en sus posibilidades y afronta la asignatura con seguridad.</p>
        <p><b>Entender.</b> Es el gran reto de todo profesor en una clase particular. Lo difícil no es aprobar, sino entender, razonar, comprender lo que nos explican. Con la atención personalizada e individualizada de una clase particular esto es posible. En una clase numerosa, es bastante más complicado.</p>
        <p><b>Rendir con eficacia en los exámenes.</b> A pesar de ir preparados, enfrentarnos a una prueba de cuyo resultado depende un aprobado o un suspenso puede jugarnos malas pasadas. En la mayoría de los casos sabemos más de lo que somos capaces de demostrar luego en los exámenes. Las clases particulares deberían servir para “entrenarnos” a afrontar esa prueba que tendremos que superar. Un buen profesor particular debería “someter” a sus alumnos a situaciones similares por las que tendrá que pasar; y esas situaciones son los exámenes que ha de superar a lo largo del curso.</p>
        <p><b>Aprobar.</b> Éste es, por supuesto, el principal objetivo, como hemos dicho, de todo estudiante en su etapa escolar. A veces un alumno, por múltiples motivos, es incapaz de aprobar una asignatura. Hace lo imposible: estudia muchas horas, mantiene la atención en clase, pregunta a su profesor incluso a algún compañero de clase del que se sabe “destaca” en esa asignatura y, sin embargo, todo resulta insuficiente. Un buen profesor particular debería siempre, contando con la colaboración, por supuesto, del alumno, ayudar a éste a aprobar la asignatura. Ése es el reto al que se enfrenta y es un objetivo que tendrá que cumplir.</p>
      </div>
      </div>
    </div>
    
    <div class="panel panel-default">
      <div class="panel-heading p-3 mb-3" role="tab" id="heading2">
      <h3 class="panel-title">
        <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
        Inconvenientes de las clases particulares
        </a>
      </h3>
      </div>
      <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
      <div class="panel-body px-3 mb-4">
        <p><b>Dependencia.</b> Recordaréis nuestro artículo ¿crean dependencia las clases particulares? en el que, efectivamente, podrían darse casos de dependencia entre alumnos que reciben clases particulares. Ése, por supuesto, sería uno de los inconvenientes. Pero ¿existen más? ¿Podemos advertir más riesgos?</p>
        <p><b>Pasividad.</b> En mundoclases hacemos hincapié en la importancia que tiene una buena actitud por parte de los alumnos en las clases que reciben en los colegios e institutos. A veces, cuando un alumno tiene un profesor particular de matemáticas, lengua o cualquier otra asignatura, ante cualquier duda mientras explica su profesor del colegio, lejos de preguntar para que sea éste quien la resuelva, espera a más tarde, cuando su profesor particular le dé la clase, para preguntar. Esa actitud hará, probablemente, que “se pierda” en el resto de la explicación y, en consecuencia, sean varias las dudas que haya de plantear luego a su profesor particular; además de demostrar una actitud pasiva en clase que en nada le beneficiará.</p>
        <p><b>Inatención.</b> Se encuentra en estrecha relación con el inconveniente anterior. Es una pasividad más acusada. El alumno, no solo no pregunta lo que no entiende en clase, sino que, como tiene un profesor particular, prefiere que sea éste quien le explique la asignatura porque ha conectado bien con él, porque le transmite más confianza, porque no le da tanta vergüenza, etc. y “pasa” de llevar a cabo algo tan necesario e importante como intentar entender a nuestros profesores de clase.</p>
        <p><b>Falta de trabajo personal.</b> ¿Para qué intentar hacer los problemas de matemáticas, de física, realizar un análisis sintáctico o entender formulación si tenemos alguien, el profesor particular, que lo hace por nosotros? Esto suele ocurrir, sobre todo, en los alumnos más pequeños. Cometemos un error cuando pedimos un profesor particular que “haga los deberes” con el alumno.</p>
        <p><b>Métodos diferentes.</b> Un profesor particular ha de saber adaptarse a las necesidades del alumno. Para ello, ha de conocer qué exige su profesor de clase así cómo qué método aplica en lo que se refiere a su asignatura: cómo explica, a qué da más o menos importancia, qué tipo de exámenes realiza, qué criterios de calificación tiene en cuenta, etc. Podéis ver nuestro artículo ¿Contamos con buenos profesores particulares?</p>
        <p><b>Exceso de complicidad.</b> A veces, sobre todo en aquellos casos en los que un alumno recibe clases particulares por parte del mismo profesor durante más de un curso, suele generarse un exceso de confianza entre profesor y alumno que genera una complicidad muy negativa para el alumno. El profesor se ha convertido casi en un miembro más de la familia, con lo que ese respeto que existía entre ambos al principio de conocerse, esa separación de roles entre los dos tan beneficiosa para el correcto aprovechamiento de las clases particulares, se va rompiendo poco a poco y ya, ni el profesor exigen todo aquello que el alumno necesita, ni el alumno se “toma en serio” y aprovecha totalmente el tiempo que pasan juntos.</p>
      </div>
      </div>
    </div>

     <div class="panel panel-default">
      <div class="panel-heading p-3 mb-3" role="tab" id="heading3">
      <h3 class="panel-title">
        <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
        ¿Están los profesores de los colegios a favor o en contra de las clases particulares?
        </a>
      </h3>
      </div>
      <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
      <div class="panel-body px-3 mb-4">
        <p>En academia olimpo, como Centros Educativos especializados en la enseñanza personaliza y, en consecuencia, en las clases particulares, no nos cansaremos de decir que, en aquellos casos en los que un alumno necesita ayuda, la colaboración entre el colegio y nosotros (o el profesor particular) resulta fundamental. Y ahí es donde comienza el problema.</p>
        <p>Muchos profesores, cuando los padres les plantean llevar a sus hijos (porque suspenden o no alcanzan la nota que creen se merecen) a clases particulares ponen el grito en el cielo. Parece como si les tocaran su amor propio, como si les hirieran en su orgullo. La respuesta suele ser siempre la misma: tú hijo/a no necesita clases; puede hacerlo por sí mismo/a. Y Claro que es casi seguro que puede. Lo que ocurre es que ese “puede” es una mezcla de realidad y deseo, de presente y futuro incierto que necesitamos solucionar porque, de lo contrario, puede acarrear serias consecuencias.</p>
        <p>El proceso de aprendizaje sabemos que es lento y complejo puesto que en él influyen de manera clara una serie de factores de muy diferente índole: capacidad, motivación, ambiente familiar, ámbito escolar, docentes, socialización en general…</p>
        <p>Sólo en aquellos casos en los que con total claridad y rotundidad absolutas se detecta que un alumno (en la mayoría de los casos por falta de capacidad) necesita apoyo escolar,  los profesores son quienes aconsejan a los padres que sus hijos reciban clases particulares. ¿Es para “quitarse el muerto” y no dedicar a ese alumno/a el tiempo y la atención que precisa? Queremos pensar que no. Pero, como hemos dicho, en un suspenso influyen otros factores que no siempre son la falta de capacidad.</p>
        <p>Clase particular es igual a atención personalizada y nosotros entendemos que en una clase de 20 ó 30 alumnos ese trabajo es del todo imposible por muy buenos profesores con los que contemos. ¿Por qué no dedicar un tiempo a averiguar si alguno de mis alumnos, atendiendo a criterios pedagógicos, necesita apoyo escolar en lugar de ser tan cortos de mente y acudir a la regla matemática?:</p>
        <p><b>Capacidad = NO</b> necesita clases particulares    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp      <b>Incapacidad = SÍ</b> necesita clases particulares</p>
        <p>Velemos por el interés del alumno, busquemos siempre su beneficio y no emitamos juicios rápidos sin antes haber analizado en profundidad si de verdad necesita o no clases particulares.</p>
      </div>
      </div>
    </div>
    
    
    <div class="panel panel-default">
      <div class="panel-heading p-3 mb-3" role="tab" id="heading4">
      <h3 class="panel-title">
        <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="true" aria-controls="collapse4">
        ¿Qué deben hacer los padres si sus hijos les piden esa ayuda?
        </a>
      </h3>
      </div>
      <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
      <div class="panel-body px-3 mb-4">
        <p>En más de una ocasión hemos defendido la teoría de que, si bien es cierto que “la educación es una tarea de todos”ha sido una premisa acuñada y aceptada desde hace siglos, en los últimos años la frase “la enseñanza es una tarea de todos” parece haberse convertido en una realidad incuestionable.</p><br>

        <h4>LOS PADRES DEBEN SUPLANTAR EN CASA LA FIGURA DEL PROFESOR DEL COLEGIO?</h4>
         <h4> ¿DEBEN HACER DE PROFESORES PARTICULARES DE SUS HIJOS?</h4><br>
         <p><b>Nuestra respuesta es NO en ambos casos.</b></p>
         <p>En primer lugar porque los padres han de ejercer precisamente de eso, de padres; tarea, por otra parte, nada fácil. La relación paterno-filial debe ser algo que las familias han de cuidar y salvaguardar. Ejercer de profesores siendo, ante todo, padres, no es imposible, pero sí complicado, muy complicado.</p>
         <p>En segundo lugar porque muy pocos padres, a pesar de que, por su formación, puedan ayudar a sus hijos, poseen algo imprescindible en toda tarea de docencia: pedagogía.</p>
         <p>Cuando damos clases particulares a nuestros hijos los tratamos, no como alumnos, sino como hijos nuestros que son. A su vez, ellos, lejos de vernos como profesores particulares, nos ven como lo que somos: sus padres. Esta realidad, lejos de ayudar, puede generar un problema añadido: discusiones, enfrentamientos, rechazo por parte de los hijos, hastío por parte de los padres…</p>
         <p>La solución, sólo para aquellos padres preparados, pasa por una mezcla de ambos papeles, para lo cual tendremos que restar algo en nuestro papel de padres y algo en nuestro papel de profesores. De esta mezcla surge la figura del tutor, de la persona que vela y tutela porque su hijo se responsabilice de su tarea como estudiante y, aconsejándole en esa dirección (que no en la de la explicación de contenidos), ambos (padres-hijos) puedan comprobar la efectividad del estudio como tarea diaria.</p>
      </div>
      </div>
    </div>
    </div>

     <div class="panel panel-default">
      <div class="panel-heading p-3 mb-3" role="tab" id="heading5">
      <h3 class="panel-title">
        <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="true" aria-controls="collapse5">
        ¿Crean dependencia?
        </a>
      </h3>
      </div>
      <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
      <div class="panel-body px-3 mb-4">
        <p>Aunque parezca una idea descabellada, no lo es en algunos casos; pudiendo afirmar que, efectivamente, las clases particulares pueden llegar a generar dependencia en algunos alumnos.</p>
        <p>Hemos trabajado con miles de alumnos en los últimos años y, en consecuencia, han sido muchas las clases particulares que hemos impartido. Esta realidad nos permite llegar a la conclusión de que, alumnos que recibieron apoyo escolar por parte de profesores particulares siendo muy pequeños, (ya en los primeros años de la etapa de Primaria) han continuado recibiendo esa ayuda hasta que han finalizado sus estudios de Secundaria y/o Bachillerato.</p>
        <p>Eran alumnos que, a las primeras de cambio, en el preciso instante en el que se encontraban con una dificultad en alguna de las asignaturas que se le “atragantaban”, demandaban por sí mismos a sus padres un profesor particular a quien consultar las dudas, alguien que les pudiera explicar lo que ya les había explicado su profesor en el colegio y no entendían y con quien poder realizar las tareas encomendadas.</p>
        <p>No hacían el esfuerzo de intentar por sí mismos resolver un problema. No preguntaban una y otra vez en las clases del colegio aquello que no entendían, ¿para qué? Siempre estaba la posibilidad de la clase particular, del profe en casa.</p>
        <p>En su yo interno se sentían incapaces de entender y aprobar la asignatura ellos solos, sin ayuda externa. No confiaban en sí mismos, “dependían” totalmente de un profesor particular. Esa ayuda constante había dejado una huella difícil de borrar en una mente que, aún preparada para la comprensión de una materia, no creía en sí misma, en su capacidad de conseguirlo sin el apoyo de unas clases particulares.</p>
      </div>
      </div>
    </div>
    

    <div class="panel panel-default">
      <div class="panel-heading p-3 mb-3" role="tab" id="heading6">
      <h3 class="panel-title">
        <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="true" aria-controls="collapse6">
        ¿Contamos con buenos profesores particulares?
        </a>
      </h3>
      </div>
      <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
      <div class="panel-body px-3 mb-4">
        <p>El oficio de profesor, aunque prefiero utilizar el término docente, es un oficio difícil, complicado, que tiene, de una parte, un componente innato y, de otra, un componente adquirido, pero adquirido con el paso del tiempo, a base de experiencias.</p>

        <br><h4>SABER MUCHO NO EQUIVALE A COMUNICAR BIEN. COMUNICAR BIEN NO EQUIVALE A SABER MUCHO.</h4><br>
        <p><b>Un buen docente, además de conocimientos adquiridos, ha de saber transmitirlos</b></p>
        <p>Además, ha de saber cómo transmitirlos dependiendo del contexto en el que esté y del alumno al que vayan dirigidos.</p>
        <p>Esto, de por sí, no es fácil, pero podemos asegurar que es más complicado aún, más difícil de conseguir, aunque parezca una contradicción, en el caso de una clase particular.</p>
        <p>Sesenta minutos o noventa, dependiendo de la duración, a solas con un alumno que reclama solución inmediata a una serie de dudas que le han surgido antes de la llegada del profesor o a medida que éste le explica tal o cual teoría, le resuelve éste o aquél problema, requiere una preparación que no muchos profesores particulares poseen.</p>
        <p>Pero, además, hay que tener en cuenta que, aunque el profesor particular prepara al alumno, al final no es quien lo examina, por lo que aspectos como los criterios de calificación del profesor titular o tipos de exámenes, etc. son aspectos a los que tendrá que prestar una gran atención.</p>
        <br><h4>SE NECESITA FORMACIÓN, EXPERIENCIA Y UN MÉTODO DIDÁCTICO QUE SEA EFICAZ.</h4><br>
        <p>Cuidado, pues, con los profesores particulares. Son muchos los que, anunciándose, pretenden serlo, pero muy pocos los que se merecen ese nombre.</p>

      </div>
      </div>
    </div>
    </div>


  
  </div>
</section>  
    


<br>
<footer>

    <div class="container-fluid mt-3">
        <div class="row jumbotron pt-4 pb-2">
            <div class="col-md-6">
                <h5 style="color:dimgray;" class="d-flex justify-content-center justify-content-md-start "><b>Dirección: Calle Luis Bayón Nº11 1ºDerecha</b></h5>
                <h5 style="color:dimgray;" class="d-flex justify-content-center justify-content-md-start "><b>Teléfono: 672 30 60 27</b></h5>

            </div>
            <div class="col-md-6">
                <ul class="list-inline d-flex justify-content-center justify-content-md-end">
                  <li class="list-inline-item"><img src="../img/faq.png"lass="img-fluid" width="40px" height="40px"><a href="faq.php">FAQ</a></li>
                    <li class="list-inline-item"><img src="../img/icon3.png"lass="img-fluid" width="40px" height="40px"><a href="https://www.facebook.com/">Facebook</a></li>
                    <li class="list-inline-item"><img src="../img/icon1.png"lass="img-fluid" width="40px" height="40px"><a href="https://www.instagram.com/">Instagram</a></li>
                    <li class="list-inline-item"><img src="../img/icon2.png"lass="img-fluid" width="40px" height="40px"><a href="https://twitter.com/">Twitter</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>



</body>

</html>
