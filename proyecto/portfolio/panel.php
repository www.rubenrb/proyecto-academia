<!DOCTYPE html>
<html>

<head>
    <title>ejercicio</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">



  <link rel="stylesheet" type="text/css" href="../vendor/bootstrap/css/bootstrap.min.css">

  <link rel="stylesheet" type="text/css" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">

  <link rel="stylesheet" type="text/css" href="../vendor/animate/animate.css">

  <link rel="stylesheet" type="text/css" href="../vendor/css-hamburgers/hamburgers.min.css">

  <link rel="stylesheet" type="text/css" href="../vendor/select2/select2.min.css">

  <link rel="stylesheet" type="text/css" href="../css/util.css">
  <link rel="stylesheet" type="text/css" href="../css/main.css">



</head>

<style>
.jumbotron-fm{
    background-color: #3F9FC8;
    color:white;

}
body{
  background-color: #EFEEED;
}
h4,b,tr,h5{
    color:#3F9FC8;
}
#negro{
  color:black;
}

#blanco{
  color:white;
}
#principal{
    background-color:#3F9FC8; 
    color:white;
    
}

.navbar{
    padding-top: 20px;
    padding-bottom: 20px;
}

footer{
padding-left: 50px;
background-color: #E7EBED;
}

.col-centered{
  display: block;
  margin-left: auto;
  margin-right: auto;
  text-align: center;
}

#texto1{
  font-family: Century Gothic, sans-serif;
}

.carousel-inner img {
      width: 100%; 
      margin: auto;
      min-height:200px;
  }

    .top {
    background-color: #f2f2f2;
    padding: 15px;
     margin-left:30px; 
     margin-right:30px;
      margin-top:10px;
    }

   

    .bot {
      
     margin-left:20px; 
     margin-right:20px;
      
    }

#navbarSupportedContent{
  font-size: 25px;
  text-align:center;
}

#columna{
 background-color:#343a40; 

}
#negrita{
  color:black;
}
#centrado{
  text-align:center;
}
h2{
  color:white;
}
input{
  font-size: 20px;
}
tr{
  text-align:center;
}
#horario{
  color:yellow;
}
#tarjeta{
  
  background-color:#efeeed;
}
#separacion{ 
padding-top: 100px; 

}
img{
display:block;
margin:auto;
}

body{padding-top:30px;}

.glyphicon {  margin-bottom: 10px;margin-right: 10px;}

small {
display: block;
line-height: 1.428571429;
color: #999;
}

</style>

<body>

   <?php
  session_start();
if(isset($_SESSION["conectado"]) && isset($_SESSION["rol"])) {

  $conexion=$_SESSION["conectado"];

  $servername = "localhost";
  $username = "secretario";
  $password = "secretario";
  $dbname = "academia";

   $conn = mysqli_connect($servername, $username, $password,$dbname);

  if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
  }


  if (isset($_POST['salir'])){

      session_destroy();
   

      header("location:../inicio.php");
  }
  if (isset($_POST['cambiar'])) {
      header('location:panel.php');
  }

  if (isset($_POST['calif'])){

      header("location:calificaciones.php");
  }
  if (isset($_POST['atras'])){

      
   

      header("location:panel.php");
  }

  if (isset($_POST['msg1'])){

      header("location:mensajes1.php");
  }
  if (isset($_POST['msg2'])){

      header("location:mensajes2.php");
  }
?>

    <div class="container-fluid mt-3">
        <div class="row">
            <div class="col">

                <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                     <a class="navbar-brand" href="../inicio.php"><img src="../images/logo.png" alt="logo"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>


                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="../inicio.php">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Inicio <span class="sr-only">(current)</span></a>
                            </li>
                            
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    &nbsp&nbsp Nuestros servicios
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                                    <p class="dropdown-item" style="background-color:powderblue;">Cursos</p>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="economia.php">Economía y empresa</a>
                                  <a class="dropdown-item" href="lengua.php">Lengua</a>
                                    
                                    
                                    <a class="dropdown-item" href="mate.php">Matemáticas</a>
                                    
                                    
                                    <a class="dropdown-item" href="ingles.php">Inglés</a>
                                    <a class="dropdown-item" href="informatica.php">Informática</a>
                                    
                                    
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="condiciones.php">&nbsp&nbsp Condiciones</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="trabajo.php">&nbsp&nbsp Espacio de trabajo</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="contacto.php">&nbsp&nbsp Contacto</a>
                            </li>
                        </ul>
                        <?php
  
                           if(!isset($_SESSION["conectado"]) && !isset($_SESSION["rol"])) {

                          ?>
                        <form class="form-inline my-2 my-lg-0">

                            <button type="button" id="boton1" class="btn btn-primary m-1" data-toggle="modal" data-target="#modalAcceso" onclick="window.location.href='form.php'">
                                Iniciar sesión

                               
                            </button>
                  
                        </form>
                        <?php } ?>


                    </div>
                </nav>

                

            </div>
        </div>

     </div>
<?php
if (!isset($_POST['edit'])) { 

  ?>
 <div id="separacion">
   <div class="container">
      <div class="row">
          <div class="col-xs-20 col-sm-10 col-md-10">
              <div class="well well-sm">
                  <div class="row">
                      <div class="col-sm-5 col-md-3">
                          <img src="../img/guest.png" alt="" class="img-rounded img-responsive" />
                      </div>
                      <div class="col-sm-6 col-md-8">
                          
                           <?php

                              $sql = "SELECT * FROM alumnos WHERE alumnoCorreo = '$conexion'";
                              $result = mysqli_query ($conn, $sql);
                              if ($result == FALSE) {

                                echo "Error en la ejecución de la consulta.<br />";

                              }else {

                                    echo "<table style='width:70%'>";
                                   

                                  while ($registro = mysqli_fetch_row($result)) {
                                    echo "<tr align='center'>";
                                    echo "<td><h3>" ."DNI "."</h3></td>";
                                    echo "<td><h3>" .$registro[0]."</h3></td>";
                                    echo "</tr>";
                                    echo "<tr>";
                                    echo "<td><h3>" ."Nombre "."</h3></td>";
                                    echo "<td><h3>" .$registro[1]."</h3></td>";
                                    echo "</tr>";
                                    echo "<tr>";
                                    echo "<td><h3>" ."Apellidos "."</h3></td>";
                                    echo "<td><h3>" .$registro[2]."</h3></td>";
                                    echo "</tr>";
                                    echo "<tr>";
                                    echo "<td><h3>" ."Teléfono "."</h3></td>";
                                    echo "<td><h3>" .$registro[3]."</h3></td>";
                                    echo "<td>"
                                    ?> 
                                    <form  action="" method="post">
                                      <input class="btn btn-link" type="submit" value="editar"  name="edit">
                                    </form>

                                      <?php
                                      echo "</td>";
                                      echo "</tr>";
                                      echo "<tr>";
                                      echo "<td><h3>" ."Correo "."</h3></td>";
                                      echo "<td><h3>" .$registro[4]."</h3></td>";
                                      echo "</tr>";
                                      echo "<tr>";
                                      echo "<td><h3>" ."Estado "."</h3></td>";
                                      echo "<td><h3>" .$registro[5]."</h3></td>";
                                      echo "</tr>";


                                  }
                                  echo "</table>"."<br>";
                              }


                                

                           ?> 
                          
                          
                            <br><br>
                                <div id="centrado">
                                  <form  action="" method="post">
                                    <input class="btn btn-dark btn-lg" type="submit" value="Ver mensajes"  name="msg1">
                                    <input class="btn btn-dark btn-lg" type="submit" value="Nuevo mensaje"  name="msg2">
                                    <input class="btn btn-success btn-lg" type="submit" value="Ver calificaciones"  name="calif">
                                    <input class="btn btn-danger btn-lg" type="submit" value="Cerrar Sesion"  name="salir">
                                  </form>

                                </div>
                          
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>  

<?php 
} 
  if (isset($_POST['edit'])) { 


?>
    <div id="separacion" text align="center">
    <img src="../img/img7.png" width="30" height="30"><h3>Escribe el nuevo número de teléfono</h3><br>
    <form action="" method="post">
    <input class="campo" name="tlf" type="text" style="height :30px;">&nbsp&nbsp

    <input class="btn btn-primary btn-lg" type="submit" value="Modificar"  name="cambiar">
    <br><br><br>
    <div id="centrado">
    <input class="btn btn-primary btn-lg" type="submit" value="Volver atras"  name="atras">
    </div>


     </form>

    </div>
    <br><br><br><br><br><br><br><br><br><br>

  <?php

  }


  if (isset($_POST['cambiar'])) { 
      $tlf=$_POST["tlf"];


      $modif = " UPDATE alumnos SET alumnoTelefono='$tlf' WHERE alumnoCorreo='$conexion'";
      $result1 = mysqli_query ($conn, $modif);

      if ($result1 == FALSE) {
        echo "Error en la ejecución de la consulta.<br />";

      }


  }

 ?>
<br><br><br><br><br><br><br><br><br><br>
<footer>

<div class="container-fluid mt-3">
        <div class="row jumbotron pt-4 pb-2">
            <div class="col-md-6">
                <h2 style="color:dimgray;" class="d-flex justify-content-center justify-content-md-start "><b>Dirección: Calle Luis Bayón Nº11 1ºDerecha</b></h2>
                <h2 style="color:dimgray;" class="d-flex justify-content-center justify-content-md-start "><b>Teléfono: 672 30 60 27</b></h2>

            </div>
            <div class="col-md-6">
                <ul class="list-inline d-flex justify-content-center justify-content-md-end">
                  <li class="list-inline-item"><img src="../img/faq.png"lass="img-fluid" width="40px" height="40px"><a href="faq.php">FAQ</a></li>
                    <li class="list-inline-item"><img src="../img/icon3.png"lass="img-fluid" width="40px" height="40px"><a href="https://www.facebook.com/">Facebook</a></li>
                    <li class="list-inline-item"><img src="../img/icon1.png"lass="img-fluid" width="40px" height="40px"><a href="https://www.instagram.com/">Instagram</a></li>
                    <li class="list-inline-item"><img src="../img/icon2.png"lass="img-fluid" width="40px" height="40px"><a href="https://twitter.com/">Twitter</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>



<?php
}
?>
 


</body>

</html>
