<!DOCTYPE html>
<html>

<head>
    <title>ejercicio</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>


</head>

<style>
.jumbotron-fm{
    background-color: #3F9FC8;
    color:white;

}
body{
  background-color: #EFEEED;
}
h4,b,tr,h5{
    color:#3F9FC8;
    text-align:center;
}
#negro{
  color:black;
}

#blanco{
  color:white;
}
#principal{
    background-color:#3F9FC8; 
    color:white;
    
}

.navbar{
    padding-top: 20px;
    padding-bottom: 20px;
}

footer{
padding-left: 50px;
background-color: #E7EBED;
}

.col-centered{
  display: block;
  margin-left: auto;
  margin-right: auto;
  text-align: center;
}

#texto1{
  font-family: Century Gothic, sans-serif;
}

.carousel-inner img {
      width: 100%; 
      margin: auto;
      min-height:200px;
  }

    .top {
    background-color: #f2f2f2;
    padding: 15px;
     margin-left:30px; 
     margin-right:30px;
      margin-top:10px;
    }

   

    .bot {
      
     margin-left:20px; 
     margin-right:20px;
      
    }

#navbarSupportedContent{
  font-size: 25px;
  text-align:center;
}

#columna{
 background-color:#343a40; 

}
#negrita{
  color:black;
}
#centrado{
  text-align:center;
}
h2{
  color:white;
}
tr{
  text-align:center;
}
#horario{
  color:yellow;
}
#tarjeta{
  
  background-color:#efeeed;
}

.grid-block{
  background:no-repeat;
  background-size: cover;
        -moz-background-size: cover;
        -webkit-background-size: cover;
        -o-background-size: cover;
  opacity: 0.5;
}
#titulo{
color:#0000FF;
  font-family: "Lucida Console", Courier, monospace;
  text-align:center;
  font-size: 50px;

}
hr{
  border: none;
  height: 3px;
  color: #333;
  background-color: #333;
}
h1{
  text-align:center;
  color:#DF0101;
}
img{
display:block;
margin:auto;
}
#negro{
  color:black;
}


</style>

<body>
  <?php
  session_start();
  ?>

   <div class="container-fluid mt-3">
        <div class="row">
            <div class="col">

                <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                     <a class="navbar-brand" href="../inicio.php"><img src="../images/logo.png" alt="logo"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>


                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="../inicio.php">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Inicio <span class="sr-only">(current)</span></a>
                            </li>
                            
                            <li class="nav-item dropdown active">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    &nbsp&nbsp Nuestros servicios
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                                    <p class="dropdown-item" style="background-color:powderblue;">Cursos</p>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="economia.php">Economía y empresa</a>
                                  <a class="dropdown-item" href="lengua.php">Lengua</a>
                                    
                                    
                                    <a class="dropdown-item" href="mate.php">Matemáticas</a>
                                    
                                    
                                    <a class="dropdown-item" href="ingles.php">Inglés</a>
                                    <a class="dropdown-item" href="informatica.php">Informática</a>
                                    
                                    
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="condiciones.php">&nbsp&nbsp Condiciones</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="trabajo.php">&nbsp&nbsp Espacio de trabajo</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="contacto.php">&nbsp&nbsp Contacto</a>
                            </li>
                        </ul>
                        <?php
  
                           if(!isset($_SESSION["conectado"]) && !isset($_SESSION["rol"])) {

                          ?>
                        <form class="form-inline my-2 my-lg-0">

                            <button type="button" id="boton1" class="btn btn-primary m-1" data-toggle="modal" data-target="#modalAcceso" onclick="window.location.href='form.php'">
                                Iniciar sesión

                               
                            </button>
                  
                        </form>
                        <?php } ?>


                    </div>
                </nav>

                

            </div>
        </div>


            
      </div>  
    
<br><br><div>

<h4>PROFESOR EXPERTO EN LAS PRINCIPALES ASIGNATURAS DE  ECONOMÍA</h4>
<h4>25 años en la docencia y formado en la Facultad de Ciencias Económicas y Empresariales</h4>
 


<br><img src="../img/20.jpg" alt="clase" class="img-fluid" width="800px" height="600px">

</div><br><br>

<div class="container">
 <ul>
  <li>MICROECONOMÍA: Oferta y Demanda, Producción y Costes, Competencia, Monopolio, Restricción presupuestaria,...</li>
  <li>MACROECONOMÍA: Contabilidad Nacional, modelo IS-LM,...</li>
  <li>EMPRESA: VAN, TIR, PERT, rentabilidad económica y financiera, tests,...</li>
  <li>FINANZAS: Métodos de selección de inversiones, estructura de capital. políticas de dividendos,..</li>
  <li>ECONOMÍA MUNDIAL: Balanza de Pagos, Ventaja comparativa, aranceles,...</li>
</ul> 


  <h2 id="negro">ASIGNATURAS Y GRADOS</h2>
  
  <table class="table table-dark table-striped">
    <thead>
      <tr>
        <th>GRADOS EN:</th>
        <th>ASIGNATURAS:</th>
        
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>ADE, ECONOMÍA, CONTABILIDAD y FINANZAS, COMERCIO Y MÁRKETING</td>
        <td>INTRODUCCIÓN A LA MICROECONOMÍA</td>
        
      </tr>
      <tr>
        <td>TURISMO, GAP, RELACIONES LABORALES, TRABAJO SOCIAL</td>
        <td>INTRODUCCIÓN A LA ECONOMÍA</td>
        
      </tr>
      <tr>
        <td>ADE, COMERCIO, CONTABILIDAD, ECONOMÍA</td>
        <td> INTRODUCCIÓN A LA MACROECONOMÍA</td>
        
      </tr>
      <tr>
        <td>BACHILLERATO, PAU, PRUEBAS DE ACCESO</td>
        <td>ECONOMIA DE LA EMPRESA</td>
        
      </tr>
      <tr>
        <td>ADE, TURISMO, COMERCIO, ECONOMÍA, CONTABILIDAD, G.A.P. RELACIONES LABORALES</td>
        <td>ECONOMÍA DE LA EMPRESA</td>
        
      </tr>
      <tr>
        <td>ADE, COMERCIO, CONTABILIDAD, ECONOMÍA</td>
        <td>ECONOMÍA DE LA EMPRESA</td>
        
      </tr>
      <tr>
        <td>ADE, DADE</td>
        <td>ANÁLISIS ECONÓMICO PARA LA EMPRESA, FINANZAS EMPRESARIALES /td>
        
      </tr>
      <tr>
        <td>INGENIERÍAS</td>
        <td>EMPRESA</td>
        
      </tr>
      <tr>
        <td>TURISMO</td>
        <td>GESTION DE LAS OPERACIONES TURISTICAS,  DIRECCIÓN FINANCIERA, MICROECONOMÍA APLICADA AL TURISMO</td>
        
      </tr>
      <tr>
        <td>RELACIONES LABORALES</td>
        <td>ECONOMÍA LABORAL</td>
        
      </tr>
      <tr>
        <td>DERECHO</td>
        <td>ECONOMÍA Y CONTABILIDAD</td>
        
      </tr>
      <tr>
        <td>GESTIÓN PÚBLICA</td>
        <td>GESTIÓN FINANCIERA ENTIDADES PÚBLICAS</td>
        
      </tr>
    </tbody>
  </table><br>
  <p>Si buscas academia o necesitas clases particulares de estas materias, ten en cuenta que todas ellas  tienen un desarrollo gráfico necesario para la comprensión de las prácticas, muchas veces de tipo test. Contamos con cientos de ellos,  exámenes  y resúmenes teóricos, todos originales del Campus y de tus propios profesores (de años anteriores). Sin practicar mucho con ellos es muy difícil superar los exámenes y hacer una buena evaluación continua.</p>

</div><br><br>


<br>
<footer>

    <div class="container-fluid mt-3">
        <div class="row jumbotron pt-4 pb-2">
            <div class="col-md-6">
                <h5 style="color:dimgray;" class="d-flex justify-content-center justify-content-md-start "><b>Dirección: Calle Luis Bayón Nº11 1ºDerecha</b></h5>
                <h5 style="color:dimgray;" class="d-flex justify-content-center justify-content-md-start "><b>Teléfono: 672 30 60 27</b></h5>

            </div>
            <div class="col-md-6">
                <ul class="list-inline d-flex justify-content-center justify-content-md-end">
                  <li class="list-inline-item"><img src="../img/faq.png"lass="img-fluid" width="40px" height="40px"><a href="faq.php">FAQ</a></li>
                    <li class="list-inline-item"><img src="../img/icon3.png"lass="img-fluid" width="40px" height="40px"><a href="https://www.facebook.com/">Facebook</a></li>
                    <li class="list-inline-item"><img src="../img/icon1.png"lass="img-fluid" width="40px" height="40px"><a href="https://www.instagram.com/">Instagram</a></li>
                    <li class="list-inline-item"><img src="../img/icon2.png"lass="img-fluid" width="40px" height="40px"><a href="https://twitter.com/">Twitter</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>




</body>

</html>
