<!DOCTYPE html>
<html>

<head>
    <title>ejercicio</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>

</head>

<style>

  
.jumbotron-fm{
    background-color: #3F9FC8;
    color:white;

}
body{
  background-color: #EFEEED;
}
h4,b,tr,h5{
    color:#3F9FC8;
}
#negro{
  color:black;
}

#blanco{
  color:white;
}
#principal{
    background-color:#3F9FC8; 
    color:white;
    
}

.navbar{
    padding-top: 20px;
    padding-bottom: 20px;
}

footer{
padding-left: 50px;
background-color: #E7EBED;
}

.col-centered{
  display: block;
  margin-left: auto;
  margin-right: auto;
  text-align: center;
}

#texto1{
  font-family: Century Gothic, sans-serif;
}

.carousel-inner img {
      width: 100%; 
      margin: auto;
      min-height:200px;
  }

    .top {
    background-color: #f2f2f2;
    padding: 15px;
     margin-left:30px; 
     margin-right:30px;
      margin-top:10px;
    }

   

    .bot {
      
     margin-left:20px; 
     margin-right:20px;
      
    }

#navbarSupportedContent{
  font-size: 25px;
  text-align:center;
}

#columna{
 background-color:#343a40; 

}
#negrita{
  color:black;
}
#centrado{
  text-align:center;
}
h2{
  color:white;
}
tr{
  text-align:center;
}
#horario{
  color:yellow;
}
#tarjeta{
  
  background-color:#efeeed;
}

img{
display:block;
margin:auto;
}

</style>

<body>
  <?php
  session_start();

if (isset($_POST['panel'])){

  if (($_SESSION["rol"])=="alumno"){

      header("location:portfolio/panel.php");
  }
  if (($_SESSION["rol"])=="profesor"){

      header("location:portfolio/panel1.php");
  }
  if (($_SESSION["rol"])=="secretario"){

      header("location:portfolio/panel2.php");
  }
}


  if (isset($_POST['salir'])){

      session_destroy();
   

      header("location:inicio.php");
  }
  
  ?>


    <div class="container-fluid mt-3">
        <div class="row">
            <div class="col">

                <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                     <a class="navbar-brand" href="inicio.php"><img src="images/logo.png" alt="logo"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>


                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="active">
                                <a class="nav-link" href="inicio.php">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Inicio <span class="sr-only">(current)</span></a>
                            </li>
                            
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    &nbsp&nbsp Nuestros servicios
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                                    <p class="dropdown-item" style="background-color:powderblue;">Cursos</p>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="portfolio/economia.php">Economía y empresa</a>
                                  <a class="dropdown-item" href="portfolio/lengua.php">Lengua</a>
                                    
                                    
                                    <a class="dropdown-item" href="portfolio/mate.php">Matemáticas</a>
                                    
                                    
                                    <a class="dropdown-item" href="portfolio/ingles.php">Inglés</a>
                                    <a class="dropdown-item" href="portfolio/informatica.php">Informática</a>
                                    
                                    
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="portfolio/condiciones.php">&nbsp&nbsp Condiciones</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="portfolio/trabajo.php">&nbsp&nbsp Espacio de trabajo</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="portfolio/contacto.php">&nbsp&nbsp Contacto</a>
                            </li>
                        </ul>
                         <?php
  
                           if(!isset($_SESSION["conectado"]) && !isset($_SESSION["rol"])) {

                          ?>
                        <form class="form-inline my-2 my-lg-0">

                            <button type="button" id="boton1" class="btn btn-primary m-1" data-toggle="modal" data-target="#modalAcceso" onclick="window.location.href='portfolio/form.php'">
                                Iniciar sesión

                               
                            </button>
                  
                        </form>
                        <?php } ?>

                    </div>
                </nav>

                

            </div>
        </div>
      </div>

 <?php
  
  if(isset($_SESSION["conectado"]) && isset($_SESSION["rol"])) {

?>

<br><br>
  <div id="centrado">
    <p>Hola <?php echo ($_SESSION['conectado']);?>, tu rol es <?php echo ($_SESSION['rol']);?> </p>
    <form  action="" method="post">
    <input class="btn btn-success" type="submit" value="Panel de control"  name="panel">
    <input class="btn btn-danger" type="submit" value="Cerrar Sesion"  name="salir">
    </form>

  </div>
<?php } ?>


          <div class="m-5">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img src="img/05.jpg" alt="img1" width="1100" height="500">
                <div class="carousel-caption">
                  <h2>Puedes consultar nuestros objetivos y estilo de trabajo.</h2>
                  <h5 id="blanco">Ya sea desde la web o acercandote a nuestra academia.</h5>
                  <p class="slide-2"><a href="portfolio/trabajo.php" class="btn btn-primary">Espacio de trabajo</a></p>
                </div>   
              </div>
              <div class="carousel-item">
                <img src="img/06.jpg" alt="img2" width="1100" height="500">
                <div class="carousel-caption">
                  <h2 id="negro">En el apartado de condiciones puedes consultar nuestras ofertas</h2>
                  <
                  <p class="slide-2"><a href="portfolio/condiciones.php" class="btn btn-primary">Condiciones</a></p>
                </div>   
              </div>
              <div class="carousel-item">
                <img src="img/11.jpg" alt="img3" width="1100" height="500">
                <div class="carousel-caption">
                  <h2 id="blanco">Si tienes alguna duda o sugerencia.</h2>
                  <h5 id="blanco">No dudes en enviarnos un mensaje con tu consulta</h5>
                  <p class="slide-2"><a href="portfolio/contacto.php" class="btn btn-primary">Contactanos</a></p>
                </div>   
              </div>

            </div>
            <a class="carousel-control-prev" href="#demo" data-slide="prev">
              <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#demo" data-slide="next">
              <span class="carousel-control-next-icon"></span>
            </a>
          </div>

          <br><br>

          <hr class="bg-light mt-5">

          <div class="clase1">
  <h1 align="center">Centro de clases particulares Olimpo</h1>
  <hr>

    



    <div class="row">
        <div class="col-md-9">

          

                <br><br><h5 align="center">Centro de Estudios Olimpo, es un centro de enseñanza ubicado en Avilés, en la Calle Luis Bayon nº11.</h5><br><br>
                <p id="texto1">Llevamos impartiendo enseñanza de calidad en Avilés desde el año 2002. Impartimos clases personalizadas a alumnos de Primaria, Secundaria, Bachillerato, Universidad , Ciclos Formativos y también preparamos Pruebas de Acceso a Ciclos Formativos y a la Universidad, así como cursos para adultos e idiomas.</p>

                <p id="texto1">Creemos que una atención personal para que los alumnos comprendan las materias y las pongan en práctica es el secreto del éxito académico, que acompañamos diariamente con un seguimiento personal de cada alumno para que el mismo obtenga muy buenos resultados.</p>

                <p id="texto1">Disponemos de un amplio horario de mañana y tarde para cubrir todas las necesidades de nuestros alumnos , y tarifas mensuales adaptadas a las necesidades de los mismos.</p>

                <p id="texto1">La formación académica es el secreto de un buen futuro profesional y por ello, formamos a nuestros alumnos pensando en su éxito futuro en el mundo laboral.</p>

                <p id="texto1">Impartimos también clases para adultos que quieren realizar pruebas de acceso a Ciclos Formativos y a la Universidad , así como clases de informática, inglés, y finanzas.</p>

                <p id="texto1">Si quieres mejorar tu rendimiento académico, confía en nosotros , obtendrás los mejores resultados.</p>

                <hr>


                 <br><br><h5 align="center">Información Adicional</h5><br><br>

                <b id="negrita"><p id="texto1">Nuestros profesores son titulados universitarios con amplia experiencia docente.</p>

                <p id="texto1">Impartimos clases de apoyo en grupos reducidos con atención individual para cada alumno.</p>

                <p id="texto1">Realizamos un seguimiento diario integral del alumno para que el mismo obtenga los mejores resultados,  apoyándole  diariamente  en  el  estudio.</p>

                <p id="texto1">Disponemos de un amplio horario flexible, el cual puedes elegir según tus necesidades y recuperar las clases  si algún dia no puedes acudir.</p>

                <p id="texto1">La  tarifa mensual corresponde al número de horas de clase a la semana, pudiendo el alumno recibir clase  en dichas horas de todas las materias que necesite, es decir, el precio de la clase es por el número de horas semanales, no por materias, por lo que es muy económico.</p></b>

                

                <hr>
                <br><br><div id="centrado">

                <p>¡¡ Reserva  tu  plaza !! </p>

                 <p> Horarios de mañana y tarde</p>

                 <p> Atención personalizada</p>

                  <p>Grupos reducidos.</p>

                 <p> 95 % de aprobados en anteriores convocatorias.</p>
                </div>
        </div> 
     
    
  

    
        <div class="col-md-3 d-none d-md-block" id="columna" align="center">

              <h2>Horario de oficina</h2>

              <p id="horario">De lunes a viernes:</p>

              <p id="horario">De 10:00 a 14:00 y 15:30 a 20:30 </p>

                <br><h2>Clases de apoyo para</h2>
                <table class="table">          
                    <tr>
                        <td>Educacion primaria</td>
                    </tr>
                    <tr >
                        <td>Secundaria</td>
                    </tr>
                    <tr >
                         <td>Bachillerato</td>
                    </tr>
                    <tr >
                        <td>Idiomas</td>
                    </tr>
                    <tr >
                        <td>Ciclos Formativos</td>
                    </tr>
                    <tr >
                        <td></td>
                    </tr>

                </table>

                <h2>Noticias</h2><br>

                <div class="card" style="width: 22rem;" id="tarjeta">
                    <div class="card-body">
                        <h5 class="card-title"><b>Comienzo del curso.</b></h5>
                        <p class="card-text">¡Bienvenidos al curso 2020/2021!<br>
                              ¡El otoño ya está aquí! Llega la primera tanda de exámenes, así que ánimo y a trabajar.
                              A partir de ahora colgaremos aquí las noticias importantes que afectan a nuestros alumnos y al plan de estudios.<br>
                              <br>¡Keep updated!</p>
    
                    </div>
                </div>

               <div class="card" style="width: 22rem;" id="tarjeta">
                    <div class="card-body">
                        <h5 class="card-title"><b>Final del curso.</b></h5>
                        <p class="card-text">El final del curso ya esta muy cerca y con ello solo queda superar los ultimos examenes antes de poder disfrutar del verano.<br><br>
                        Así que mucha suerte a todos con vuestros examenes</p>

    
                    </div>
                </div><br><br>
                
      
      </div>  
    
   

 </div>

<br>
<footer>

<div class="container-fluid mt-3">
        <div class="row jumbotron pt-4 pb-2">
            <div class="col-md-6">
                <h5 style="color:dimgray;" class="d-flex justify-content-center justify-content-md-start "><b>Dirección: Calle Luis Bayón Nº11 1ºDerecha</b></h5>
                <h5 style="color:dimgray;" class="d-flex justify-content-center justify-content-md-start "><b>Teléfono: 672 30 60 27</b></h5>

            </div>
            <div class="col-md-6">
                <ul class="list-inline d-flex justify-content-center justify-content-md-end">
                    <li class="list-inline-item"><img src="img/faq.png"lass="img-fluid" width="40px" height="40px"><a href="portfolio/faq.php">FAQ</a></li>
                    <li class="list-inline-item"><img src="img/icon3.png"lass="img-fluid" width="40px" height="40px"><a href="https://www.facebook.com/">Facebook</a></li>
                    <li class="list-inline-item"><img src="img/icon1.png"lass="img-fluid" width="40px" height="40px"><a href="https://www.instagram.com/">Instagram</a></li>
                    <li class="list-inline-item"><img src="img/icon2.png"lass="img-fluid" width="40px" height="40px"><a href="https://twitter.com/">Twitter</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
</div>




</body>

</html>
